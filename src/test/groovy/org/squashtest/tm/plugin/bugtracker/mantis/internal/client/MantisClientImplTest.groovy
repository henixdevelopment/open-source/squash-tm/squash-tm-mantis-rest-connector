/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.client

import org.springframework.context.MessageSource
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestTemplate
import org.squashtest.csp.core.bugtracker.core.BugTrackerNoCredentialsDetailedException
import org.squashtest.csp.core.bugtracker.core.BugTrackerRemoteException
import org.squashtest.tm.domain.bugtracker.BugTracker
import org.squashtest.tm.domain.servers.TokenAuthCredentials
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.exception.ExceptionHandler
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Issue
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.MantisConfig
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.MantisFiles
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Project
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.response.MantisConfigsResponse
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.response.MantisIssueResponse
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.response.MantisIssuesResponse
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.response.MantisProjectsResponse
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.template.LabelTemplate
import org.squashtest.tm.plugin.bugtracker.mantis.internal.utils.RestTemplateFactory
import spock.lang.Specification

class MantisClientImplTest extends Specification {
    RestTemplateFactory restTemplateFactory
    ExceptionHandler exceptionHandler
    MantisClientImpl mantisClient
    MessageSource messageSource
    CredentialHolder credentialHolder
    TokenAuthCredentials tokenAuthCredentials
    BugTracker bugTracker
    RestTemplate restTemplate

    def setup() {
        bugTracker = Mock()
        restTemplateFactory = Mock()
        restTemplate = Mock()
        messageSource = Mock()
        credentialHolder = Mock()
        tokenAuthCredentials = new TokenAuthCredentials("test")
        exceptionHandler = new ExceptionHandler(messageSource)
        mantisClient = new MantisClientImpl(restTemplateFactory, exceptionHandler)
    }

    def "Should not fail while initializing"() {
        given:
        credentialHolder.getCredentials() >> tokenAuthCredentials
        restTemplateFactory.restTemplate(bugTracker) >> restTemplate
        ResponseEntity entity = Mock()
        restTemplate.exchange("/api/rest/users/me" as String, HttpMethod.GET, _ as HttpEntity<?>, _ as ParameterizedTypeReference<Object>) >> entity

        when:
        mantisClient.init(bugTracker, credentialHolder)

        then:
        noExceptionThrown()
        1 * entity.getBody()
    }

    def "Should fail while initializing null response"() {
        given:
        credentialHolder.getCredentials() >> tokenAuthCredentials
        restTemplateFactory.restTemplate(bugTracker) >> restTemplate
        restTemplate.exchange("/api/rest/users/me" as String, HttpMethod.GET, _ as HttpEntity<?>, _ as ParameterizedTypeReference<Object>) >> null

        when:
        mantisClient.init(bugTracker, credentialHolder)

        then:
        thrown(BugTrackerRemoteException)
    }

    def "Should fail while initializing incorrect token"() {
        given:
        credentialHolder.getCredentials() >> tokenAuthCredentials
        restTemplateFactory.restTemplate(bugTracker) >> restTemplate
        Exception e = GroovyMock(HttpClientErrorException.Forbidden)

        when:
        mantisClient.init(bugTracker, credentialHolder)

        then:
        1 * restTemplate.exchange("/api/rest/users/me" as String, HttpMethod.GET, _ as HttpEntity<?>, _ as ParameterizedTypeReference<Object>) >> { throw e }
        thrown(BugTrackerNoCredentialsDetailedException)

    }

    def "Should fail when sending bad request"() {
        given:
        credentialHolder.getCredentials() >> tokenAuthCredentials
        restTemplateFactory.restTemplate(bugTracker) >> restTemplate
        HttpClientErrorException e = Mock(HttpClientErrorException)

        when:
        mantisClient.init(bugTracker, credentialHolder)

        then:
        1 * e.getResponseBodyAsString() >>   '"test" : "test"'
        1 * restTemplate.exchange("/api/rest/users/me" as String, HttpMethod.GET, _ as HttpEntity<?>, _ as ParameterizedTypeReference<Object>) >> { throw e }
        thrown(BugTrackerRemoteException)

    }

    def "Should fail when searching a non existant project"() {
        given:
        credentialHolder.getCredentials() >> tokenAuthCredentials
        restTemplateFactory.restTemplate(bugTracker) >> restTemplate
        Exception e = GroovyMock(HttpClientErrorException.NotFound)
        mantisClient.restTemplate = restTemplate

        when:
        mantisClient.findProjectById("0", credentialHolder)

        then:
        1 * restTemplate.exchange("/api/rest/projects/0" as String, HttpMethod.GET, _ as HttpEntity<?>, ParameterizedTypeReference.forType(MantisProjectsResponse.class)) >> { throw e }
        thrown(BugTrackerRemoteException)

    }

    def "Should fail while creating issue with incorrect token"() {
        given:
        credentialHolder.getCredentials() >> tokenAuthCredentials
        restTemplateFactory.restTemplate(bugTracker) >> restTemplate
        Issue issue = new Issue("1")
        Exception e = GroovyMock(HttpClientErrorException.Forbidden)
        mantisClient.restTemplate = restTemplate

        when:
        mantisClient.createIssue(issue, credentialHolder)

        then:
        1 * restTemplate.exchange("/api/rest/issues" as String, HttpMethod.POST, _ as HttpEntity<?>, _ as ParameterizedTypeReference<MantisIssueResponse>) >> { throw e }
        thrown(BugTrackerNoCredentialsDetailedException)

    }

    def "Should fail while uploading attachement issue with incorrect token"() {
        given:
        credentialHolder.getCredentials() >> tokenAuthCredentials
        restTemplateFactory.restTemplate(bugTracker) >> restTemplate
        Exception e = GroovyMock(HttpClientErrorException.Forbidden)
        mantisClient.restTemplate = restTemplate

        when:
        mantisClient.uploadFiles("1", Mock(MantisFiles), credentialHolder)

        then:
        1 * restTemplate.exchange("/api/rest/issues/1/files" as String, HttpMethod.POST, _ as HttpEntity<?>, _ as ParameterizedTypeReference<MantisIssueResponse>) >> { throw e }
        thrown(BugTrackerNoCredentialsDetailedException)

    }

    def "Should return a mantis issue response when searching an issue by id"() {
        given:
        ResponseEntity entity = Mock()
        credentialHolder.getCredentials() >> tokenAuthCredentials
        MantisIssuesResponse issuesResponse = new MantisIssuesResponse()
        List<Issue> issues = Arrays.asList(new Issue("1"))
        issuesResponse.setIssues(issues)
        mantisClient.restTemplate = restTemplate

        when:
        def result = mantisClient.findIssues(1L, credentialHolder)

        then:
        1 * restTemplate.exchange("/api/rest/issues/1" as String, HttpMethod.GET, _ as HttpEntity<?>, _ as ParameterizedTypeReference<Object>) >> entity
        1 * entity.getBody() >> issuesResponse
        result in MantisIssuesResponse
        result.getIssues().size() == 1
        noExceptionThrown()
    }

    def "Should return a mantis project response"() {
        given:
        ResponseEntity entity = Mock()
        credentialHolder.getCredentials() >> tokenAuthCredentials
        MantisProjectsResponse projectsResponse = new MantisProjectsResponse()
        List<Project> projects = Arrays.asList(new Project("1", "project"))
        projectsResponse.setProjects(projects)
        mantisClient.restTemplate = restTemplate

        when:
        def result = mantisClient.findProjectById("1", credentialHolder)

        then:
        1 * restTemplate.exchange("/api/rest/projects/1" as String, HttpMethod.GET, _ as HttpEntity<?>, _ as ParameterizedTypeReference<Object>) >> entity
        1 * entity.getBody() >> projectsResponse
        result in MantisProjectsResponse
        result.getProjects().size() == 1
        noExceptionThrown()
    }

    def "Should return a mantis project response containing all projects"() {
        given:
        ResponseEntity entity = Mock()
        credentialHolder.getCredentials() >> tokenAuthCredentials
        MantisProjectsResponse projectsResponse = new MantisProjectsResponse()
        List<Project> projects = Arrays.asList(new Project("1", "project"), new Project("2", "project2"), new Project("3", "project3"))
        projectsResponse.setProjects(projects)
        mantisClient.restTemplate = restTemplate

        when:
        def result = mantisClient.findAllProjects(credentialHolder)

        then:
        1 * restTemplate.exchange("/api/rest/projects" as String, HttpMethod.GET, _ as HttpEntity<?>, _ as ParameterizedTypeReference<Object>) >> entity
        1 * entity.getBody() >> projectsResponse
        result in MantisProjectsResponse
        result.getProjects().size() == 3
        noExceptionThrown()
    }

    def "Should return a mantis config response containing all severities"() {
        given:
        ResponseEntity entity = Mock()
        credentialHolder.getCredentials() >> tokenAuthCredentials
        MantisConfigsResponse configsResponse = new MantisConfigsResponse()
        List<MantisConfig> mantisConfigs = Arrays.asList(Arrays.asList(new LabelTemplate(), new LabelTemplate(), new LabelTemplate()) as MantisConfig)
        configsResponse.setConfigs(mantisConfigs)
        mantisClient.restTemplate = restTemplate

        when:
        def result = mantisClient.getSeverities(credentialHolder)

        then:
        1 * restTemplate.exchange("/api/rest/config?option=severity_enum_string" as String, HttpMethod.GET, _ as HttpEntity<?>, _ as ParameterizedTypeReference<Object>) >> entity
        1 * entity.getBody() >> configsResponse
        result in MantisConfigsResponse
        result.getConfigs().get(0).getValues().size() == 3
        noExceptionThrown()
    }

    def "Should return a mantis config response containing all priorities"() {
        given:
        ResponseEntity entity = Mock()
        credentialHolder.getCredentials() >> tokenAuthCredentials
        MantisConfigsResponse configsResponse = new MantisConfigsResponse()
        List<MantisConfig> mantisConfigs = Arrays.asList(Arrays.asList(new LabelTemplate(), new LabelTemplate(), new LabelTemplate()) as MantisConfig)
        configsResponse.setConfigs(mantisConfigs)
        mantisClient.restTemplate = restTemplate

        when:
        def result = mantisClient.getPriorities(credentialHolder)

        then:
        1 * restTemplate.exchange("/api/rest/config?option=priority_enum_string" as String, HttpMethod.GET, _ as HttpEntity<?>, _ as ParameterizedTypeReference<Object>) >> entity
        1 * entity.getBody() >> configsResponse
        result in MantisConfigsResponse
        result.getConfigs().get(0).getValues().size() == 3
        noExceptionThrown()
    }

    def "Should return a mantis config response containing all reproducibilities"() {
        given:
        ResponseEntity entity = Mock()
        credentialHolder.getCredentials() >> tokenAuthCredentials
        MantisConfigsResponse configsResponse = new MantisConfigsResponse()
        List<MantisConfig> mantisConfigs = Arrays.asList(Arrays.asList(new LabelTemplate(), new LabelTemplate(), new LabelTemplate()) as MantisConfig)
        configsResponse.setConfigs(mantisConfigs)
        mantisClient.restTemplate = restTemplate

        when:
        def result = mantisClient.getReproducibilities(credentialHolder)

        then:
        1 * restTemplate.exchange("/api/rest/config?option=reproducibility_enum_string" as String, HttpMethod.GET, _ as HttpEntity<?>, _ as ParameterizedTypeReference<Object>) >> entity
        1 * entity.getBody() >> configsResponse
        result in MantisConfigsResponse
        result.getConfigs().get(0).getValues().size() == 3
        noExceptionThrown()
    }

    def "Should return a mantis issue response when creating an issue"() {
        given:
        ResponseEntity entity = Mock()
        credentialHolder.getCredentials() >> tokenAuthCredentials
        MantisIssueResponse issueResponse = new MantisIssueResponse()
        Issue issue = new Issue("1")
        issueResponse.setIssue(issue)
        mantisClient.restTemplate = restTemplate

        when:
        def result = mantisClient.createIssue(issue, credentialHolder)

        then:
        1 * restTemplate.exchange("/api/rest/issues" as String, HttpMethod.POST, _ as HttpEntity<?>, _ as ParameterizedTypeReference<Object>) >> entity
        1 * entity.getBody() >> issueResponse
        result in MantisIssueResponse
        result.getIssue().getId() == "1"
        noExceptionThrown()
    }
}
