/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal

import org.springframework.context.MessageSource
import org.squashtest.tm.plugin.bugtracker.mantis.MantisBugtrackerConnectorInterfaceDescriptor
import spock.lang.Specification

class MantisBugtrackerConnectorInterfaceDescriptorTest extends Specification {

    Locale locale
    MantisBugtrackerConnectorInterfaceDescriptor interfaceDescriptor
    MessageSource messageSource

    def setup() {
        messageSource = Mock()
        locale = new Locale("en")
        interfaceDescriptor = new MantisBugtrackerConnectorInterfaceDescriptor(
                messageSource
        )
    }

    def "Should setting locale"() {
        given:
        Locale newLocale = new Locale("fr")

        when:
        interfaceDescriptor.setLocale(newLocale)

        then:
        interfaceDescriptor.getLocale() == newLocale
        noExceptionThrown()
    }

    def "Should return locale"() {
        when:
        def result = interfaceDescriptor.getLocale()

        then:
        result == interfaceDescriptor.locale
        noExceptionThrown()
    }

    def "Should return false supports rich description"() {

        when:
        def result = interfaceDescriptor.getSupportsRichDescription()

        then:
        noExceptionThrown()
        !result
    }

    def "Should return false supports rich comment"() {

        when:
        def result = interfaceDescriptor.getSupportsRichComment()

        then:
        noExceptionThrown()
        !result
    }

    def "Should return string when getting report priority label"() {

        when:
        def result = interfaceDescriptor.getReportPriorityLabel()

        then:
        1 * interfaceDescriptor.messageSource.getMessage(
                "interface.report.priority.label", null, _
        ) >> "label"
        noExceptionThrown()
        result in String
    }

    def "Should return string when getting report version label"() {

        when:
        def result = interfaceDescriptor.getReportVersionLabel()

        then:
        1 * interfaceDescriptor.messageSource.getMessage(
                "interface.report.version.label", null, _
        ) >> "label"
        noExceptionThrown()
        result in String
    }

    def "Should return string when getting assignee version label"() {

        when:
        def result = interfaceDescriptor.getReportAssigneeLabel()

        then:
        1 * interfaceDescriptor.messageSource.getMessage(
                "interface.report.assignee.label", null, _
        ) >> "label"
        noExceptionThrown()
        result in String
    }

    def "Should return string when getting category version label"() {

        when:
        def result = interfaceDescriptor.getReportCategoryLabel()

        then:
        1 * interfaceDescriptor.messageSource.getMessage(
                "interface.report.category.label", null, _
        ) >> "label"
        noExceptionThrown()
        result in String
    }

    def "Should return string when getting summary version label"() {

        when:
        def result = interfaceDescriptor.getReportSummaryLabel()

        then:
        1 * interfaceDescriptor.messageSource.getMessage(
                "interface.report.summary.label", null, _
        ) >> "label"
        noExceptionThrown()
        result in String
    }

    def "Should return string when getting description version label"() {

        when:
        def result = interfaceDescriptor.getReportDescriptionLabel()

        then:
        1 * interfaceDescriptor.messageSource.getMessage(
                "interface.report.description.label", null, _
        ) >> "label"
        noExceptionThrown()
        result in String
    }

    def "Should return string when getting comment version label"() {

        when:
        def result = interfaceDescriptor.getReportCommentLabel()

        then:
        1 * interfaceDescriptor.messageSource.getMessage(
                "interface.report.comment.label", null, _
        ) >> "label"
        noExceptionThrown()
        result in String
    }

    def "Should return string when getting empty version label"() {

        when:
        def result = interfaceDescriptor.getEmptyVersionListLabel()

        then:
        1 * interfaceDescriptor.messageSource.getMessage(
                "interface.report.lists.emptyversion.label", null, _
        ) >> "label"
        noExceptionThrown()
        result in String
    }

    def "Should return string when getting empty category label"() {

        when:
        def result = interfaceDescriptor.getEmptyCategoryListLabel()

        then:
        1 * interfaceDescriptor.messageSource.getMessage(
                "interface.report.lists.emptycategory.label", null, _
        ) >> "label"
        noExceptionThrown()
        result in String
    }

    def "Should return string when getting empty assignee label"() {

        when:
        def result = interfaceDescriptor.getEmptyAssigneeListLabel()

        then:
        1 * interfaceDescriptor.messageSource.getMessage(
                "interface.report.lists.emptyassignee.label", null, _
        ) >> "label"
        noExceptionThrown()
        result in String
    }

    def "Should return string when getting table issue id header"() {

        when:
        def result = interfaceDescriptor.getTableIssueIDHeader()

        then:
        1 * interfaceDescriptor.messageSource.getMessage(
                "interface.table.issueid.header", null, _
        ) >> "label"
        noExceptionThrown()
        result in String
    }

    def "Should return string when getting table summary header"() {

        when:
        def result = interfaceDescriptor.getTableSummaryHeader()

        then:
        1 * interfaceDescriptor.messageSource.getMessage(
                "interface.table.summary.header", null, _
        ) >> "label"
        noExceptionThrown()
        result in String
    }

    def "Should return string when getting table priority header"() {

        when:
        def result = interfaceDescriptor.getTablePriorityHeader()

        then:
        1 * interfaceDescriptor.messageSource.getMessage(
                "interface.table.priority.header", null, _
        ) >> "label"
        noExceptionThrown()
        result in String
    }

    def "Should return string when getting table status header"() {

        when:
        def result = interfaceDescriptor.getTableStatusHeader()

        then:
        1 * interfaceDescriptor.messageSource.getMessage(
                "interface.table.status.header", null, _
        ) >> "label"
        noExceptionThrown()
        result in String
    }

    def "Should return string when getting table description header"() {

        when:
        def result = interfaceDescriptor.getTableDescriptionHeader()

        then:
        1 * interfaceDescriptor.messageSource.getMessage(
                "interface.table.description.header", null, _
        ) >> "label"
        noExceptionThrown()
        result in String
    }

    def "Should return string when getting table assignee header"() {

        when:
        def result = interfaceDescriptor.getTableAssigneeHeader()

        then:
        1 * interfaceDescriptor.messageSource.getMessage(
                "interface.table.assignee.header", null, _
        ) >> "label"
        noExceptionThrown()
        result in String
    }

    def "Should return string when getting table reportedIn header"() {

        when:
        def result = interfaceDescriptor.getTableReportedInHeader()

        then:
        1 * interfaceDescriptor.messageSource.getMessage(
                "interface.table.reportedin.header", null, _
        ) >> "label"
        noExceptionThrown()
        result in String
    }

    def "Should return string when getting table no assignee header"() {

        when:
        def result = interfaceDescriptor.getTableNoAssigneeLabel()

        then:
        1 * interfaceDescriptor.messageSource.getMessage(
                "interface.table.null.assignee.label", null, _
        ) >> "label"
        noExceptionThrown()
        result in String
    }

}
