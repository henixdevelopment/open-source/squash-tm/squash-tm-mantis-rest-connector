/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.conversion

import org.squashtest.tm.bugtracker.advanceddomain.AdvancedIssue
import org.squashtest.tm.bugtracker.advanceddomain.FieldValue
import org.squashtest.tm.bugtracker.definition.RemoteIssue
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Issue
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Project
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.ProjectCustomField
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.project.customfields.CustomFieldsType
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.mantisissue.SquashToMantisIssueConverter
import spock.lang.Specification

class SquashToMantisIssueConverterTest extends Specification {
    def "Should return issue when converting from advanced issue"() {
        given:
        RemoteIssue remoteIssue = new AdvancedIssue()
        remoteIssue.setFieldValue("category", new FieldValue())
        remoteIssue.setFieldValue("severity", new FieldValue())
        remoteIssue.setFieldValue("assignee", new FieldValue())
        remoteIssue.setFieldValue("summary", new FieldValue())
        remoteIssue.setFieldValue("description", new FieldValue())
        remoteIssue.setFieldValue("additionalInformation", new FieldValue())
        remoteIssue.setFieldValue("version", new FieldValue())
        remoteIssue.getFieldValue("version").setId("hello")
        remoteIssue.setFieldValue("priority", new FieldValue())
        remoteIssue.setFieldValue("reproducibility", new FieldValue())
        remoteIssue.setFieldValue("stepsToReproduce", new FieldValue())
        remoteIssue.setFieldValue("viewState", new FieldValue())
        remoteIssue.getFieldValue("viewState").setScalar("false")
        remoteIssue.setFieldValue("tags", new FieldValue())
        remoteIssue.setFieldValue(CustomFieldsType.NUMERIC_FIELD, new FieldValue("1", "11"))
        remoteIssue.setFieldValue(CustomFieldsType.CHECKBOX_FIELD, new FieldValue("2", "toto,titi,tata"))
        FieldValue f1 = new FieldValue("3", "toto")
        FieldValue f2 = new FieldValue("4", "tata")
        FieldValue f3 = new FieldValue("5", "titi")
        FieldValue[] fieldValues = [f1, f2, f3]
        FieldValue fieldValue = new FieldValue()
        fieldValue.setComposite(fieldValues)
        remoteIssue.setFieldValue(CustomFieldsType.MULTI_SELECTION_FIELD, fieldValue)
        remoteIssue.setFieldValue(CustomFieldsType.DATE_FIELD, new FieldValue("6", "2222-12-23"))
        Project project = new Project("1", "test")
        List<ProjectCustomField> customFields = Arrays.asList(new ProjectCustomField(CustomFieldsType.DATE_FIELD, CustomFieldsType.DATE_FIELD),
                new ProjectCustomField(CustomFieldsType.MULTI_SELECTION_FIELD, CustomFieldsType.MULTI_SELECTION_FIELD),
                new ProjectCustomField(CustomFieldsType.CHECKBOX_FIELD, CustomFieldsType.CHECKBOX_FIELD),
                new ProjectCustomField(CustomFieldsType.NUMERIC_FIELD, CustomFieldsType.NUMERIC_FIELD),)
        project.setCustomFields(customFields)

        when:
        def result = SquashToMantisIssueConverter.convert(remoteIssue, project)

        then:
        noExceptionThrown()
        result in Issue
    }
}
