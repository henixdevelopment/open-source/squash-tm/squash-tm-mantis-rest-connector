/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.domain

import org.springframework.context.MessageSource
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Category
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Priority
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Project
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.ProjectCustomField
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Reproducibility
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Severity
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Version
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.template.LabelTemplate
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.project.customfields.CustomFieldsType
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.project.customfields.SchemeBuilder
import spock.lang.Specification

class SchemeBuilderTest extends Specification{


    def "should return scheme"() {

        given:
        MessageSource messageSource = Mock()
        SchemeBuilder schemeBuilder = new SchemeBuilder(messageSource)
        Project project = new Project("1", "project")

        List<LabelTemplate> severities = new ArrayList<>(Arrays.asList(
                new Severity("1","severity1"),
                new Severity("2","severity2")
        ))
        List<LabelTemplate> priorities = new ArrayList<>(Arrays.asList(
                new Priority("priority1"),
                new Priority("priority2")
        ))
        List<LabelTemplate> reproductibilities = new ArrayList<>(Arrays.asList(
                new Reproducibility("reproducibility1"),
                new Reproducibility(SchemeBuilder.HAVE_NOT_TRIED_OPTION),
        ))
        List<Category> categories = new ArrayList<>(Arrays.asList(
                new Category("category1"),
                new Category("category2")
        ))
        List<Version> versions = new ArrayList<>(Arrays.asList(
                new Version("version1"),
                new Version("version2")
        ))
        List<ProjectCustomField> customFields = new ArrayList<>(Arrays.asList(
                new ProjectCustomField(CustomFieldsType.TEXT_FIELD, "text"),
                new ProjectCustomField(CustomFieldsType.EMAIL_FIELD, "email"),
                new ProjectCustomField(CustomFieldsType.NUMERIC_FIELD, "num"),
                new ProjectCustomField(CustomFieldsType.FLOAT_FIELD, "float"),
                new ProjectCustomField(CustomFieldsType.ENUM_FIELD, "enum", "test|test"),
                new ProjectCustomField(CustomFieldsType.LIST_FIELD, "list", "test|test"),
                new ProjectCustomField(CustomFieldsType.RADIO_FIELD, "radio", "test|test"),
                new ProjectCustomField(CustomFieldsType.CHECKBOX_FIELD, "checkbox", "test|test"),
                new ProjectCustomField(CustomFieldsType.MULTI_SELECTION_FIELD, "multi", "test|test"),
                new ProjectCustomField(CustomFieldsType.DATE_FIELD, "date"),
                new ProjectCustomField(CustomFieldsType.TEXTAREA_FIELD, "area")
        ))
        ProjectCustomField requiredCUF = new ProjectCustomField(CustomFieldsType.TEXT_FIELD, "requiredText")
        requiredCUF.setRequireReport(true)
        customFields.add(requiredCUF)
        project.setCustomFields(customFields)
        project.setVersions(versions)
        project.setCategories(categories)
        int defaultFieldsNumber = 13
        int totalFields = defaultFieldsNumber + customFields.size()

        when:
        def result = schemeBuilder.buildSchemes(project, severities, priorities, reproductibilities)

        then:
        result in Map
        result.get("Issue").size() == totalFields
        noExceptionThrown()
    }
}
