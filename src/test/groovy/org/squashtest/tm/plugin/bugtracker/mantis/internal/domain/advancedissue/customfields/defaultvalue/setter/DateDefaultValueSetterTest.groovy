/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.advancedissue.customfields.defaultvalue.setter

import org.slf4j.Logger
import org.squashtest.tm.bugtracker.advanceddomain.AdvancedIssue
import org.squashtest.tm.bugtracker.advanceddomain.FieldValue
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.ProjectCustomField
import spock.lang.Specification

import java.lang.reflect.Field
import java.lang.reflect.Modifier

class DateDefaultValueSetterTest extends Specification{

    def "Should convert #source date to #target milliseconds"() {
        given:
        DateDefaultValueSetter dateFieldValueSetter = new DateDefaultValueSetter()
        def projectCustomField = new ProjectCustomField("date", "date")
        projectCustomField.setDefaultValue(source)
        def issue = new AdvancedIssue()

        when:
        dateFieldValueSetter.setDefaultValue(projectCustomField, issue)

        then:
        issue.getFieldValue("date").getScalar() == target

        where:
        source       | target
        "2000-12-03" | "2000-12-03"
        "2000/12/03" | "2000-12-03"
        "20001203"   | "2000-12-03"
        "03-12-2000" | "2000-12-03"
    }

    def "Should not set field value if date format is not supported "() {
        given:
        DateDefaultValueSetter dateFieldValueSetter = new DateDefaultValueSetter()
        def projectCustomField = new ProjectCustomField("date", "date")
        projectCustomField.setDefaultValue("2000_12_03")
        def issue = new AdvancedIssue()

        when:
        dateFieldValueSetter.setDefaultValue(projectCustomField, issue)

        then:
        0 * issue.setFieldValue("date", _ as FieldValue)
    }

    def "Should not set field value if date is not a real date"() {
        given:
        DateDefaultValueSetter dateFieldValueSetter = new DateDefaultValueSetter()
        def projectCustomField = new ProjectCustomField("date", "date")
        projectCustomField.setDefaultValue("test")
        def issue = new AdvancedIssue()

        when:
        dateFieldValueSetter.setDefaultValue(projectCustomField, issue)

        then:
        0 * issue.setFieldValue("date", _ as FieldValue)
    }

    def "Should do nothing if default value is empty"() {
        given:
        DateDefaultValueSetter dateFieldValueSetter = new DateDefaultValueSetter()
        def projectCustomField = new ProjectCustomField("date", "date")
        projectCustomField.setDefaultValue("")
        def issue = new AdvancedIssue()

        when:
        dateFieldValueSetter.setDefaultValue(projectCustomField, issue)

        then:
        0 * issue.setFieldValue("date", _ as FieldValue)
        noExceptionThrown()
    }
}
