/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal

import org.squashtest.csp.core.bugtracker.core.BugTrackerRemoteException
import org.squashtest.tm.bugtracker.advanceddomain.AdvancedIssue
import org.squashtest.tm.bugtracker.advanceddomain.DelegateCommand
import org.squashtest.tm.bugtracker.definition.RemoteIssue
import org.squashtest.tm.bugtracker.definition.context.RemoteIssueContext
import org.squashtest.tm.domain.bugtracker.BugTracker
import org.squashtest.tm.domain.servers.AuthenticationProtocol
import org.squashtest.tm.domain.servers.Credentials
import org.squashtest.tm.domain.servers.TokenAuthCredentials
import org.squashtest.tm.plugin.bugtracker.mantis.MantisBugtrackerConnector
import org.squashtest.tm.plugin.bugtracker.mantis.MantisBugtrackerConnectorInterfaceDescriptor
import org.squashtest.tm.plugin.bugtracker.mantis.internal.service.MantisService
import spock.lang.Specification

import static org.squashtest.tm.domain.servers.AuthenticationProtocol.TOKEN_AUTH

class MantisBugtrackerConnectorTest extends Specification {
    private MantisBugtrackerConnector bugtrackerConnector
    private MantisBugtrackerConnectorInterfaceDescriptor interfaceDescriptor
    private MantisService mantisService

    def setup() {
        mantisService = Mock()
        interfaceDescriptor = Mock()
        bugtrackerConnector = new MantisBugtrackerConnector(interfaceDescriptor, mantisService)
    }

    def "Should return token authorization"() {
        given:
        AuthenticationProtocol[] protocols = [TOKEN_AUTH]

        when:
        def result = bugtrackerConnector.getSupportedAuthProtocols()

        then:
        result == protocols
    }

    def "Should return url of view issue"() {
        given:
        bugtrackerConnector.bugtracker = Mock(BugTracker)

        when:
        def result = bugtrackerConnector.makeViewIssueUrl("1")

        then:
        2 * bugtrackerConnector.bugtracker.getUrl() >> "http://localhost:8989"
        result.toString() == "http://localhost:8989/view.php?id=1"
    }

    def "Should return null url"() {
        given:
        bugtrackerConnector.bugtracker = Mock(BugTracker)

        when:
        def result = bugtrackerConnector.makeViewIssueUrl("1")

        then:
        1 * bugtrackerConnector.bugtracker.getUrl() >> null
        result == null
    }

    def "Should fail while returning url of view issue"() {
        given:
        bugtrackerConnector.bugtracker = Mock(BugTracker)

        when:
        bugtrackerConnector.makeViewIssueUrl("1")

        then:
        1 * bugtrackerConnector.bugtracker.getUrl() >> { throw new MalformedURLException() }
        thrown(BugTrackerRemoteException)
    }

    def "Should return null project"() {
        when:
        def result = bugtrackerConnector.findProject("1")

        then:
        result == null
    }

    def "Should return null project when finding project by id"() {
        when:
        def result = bugtrackerConnector.findProjectById("1")

        then:
        result == null
    }

    def "Should return an advanced issue when creating issue"() {
        given:
        RemoteIssue issue = Mock(AdvancedIssue)

        when:
        def result = bugtrackerConnector.createIssue(issue)

        then:
        1 * mantisService.createIssue(issue, _) >> new AdvancedIssue()
        result in AdvancedIssue
    }

    def "Should return null issue when finding issue"() {
        when:
        def result = bugtrackerConnector.findIssue("1")

        then:
        result == null
    }

    def "Should return a remote issue when creating report issue template"() {
        when:
        def result = bugtrackerConnector.createReportIssueTemplate(_ as String, Mock(RemoteIssueContext))

        then:
        1 * mantisService.createReportIssueTemplate(_, _, _, _) >> new AdvancedIssue()
        result in RemoteIssue
    }

    def "Should return a list of issues when searching issues"() {
        when:
        def result = bugtrackerConnector.findIssues(_ as ArrayList<String>)

        then:
        1 * mantisService.findKnownIssues(_, _) >> new ArrayList<AdvancedIssue>()
        result in List
    }

    def "Should return null object when executing delegate command"() {
        when:
        def result = bugtrackerConnector.executeDelegateCommand(Mock(DelegateCommand))

        then:
        result == null
    }

    def "Should return interface descriptor object when getting interface descriptor"() {
        when:
        def result = bugtrackerConnector.getInterfaceDescriptor()

        then:
        result == interfaceDescriptor
    }

    def "Should not fail when authenticating"() {
        given:
        Credentials credentials = Mock(TokenAuthCredentials)
        bugtrackerConnector.bugtracker = Mock(BugTracker)

        when:
        bugtrackerConnector.authenticate(credentials as Credentials)

        then:
        1 * credentials.getImplementedProtocol() >> TOKEN_AUTH
        1 * mantisService.init(bugtrackerConnector.bugtracker, bugtrackerConnector.credentialHolder) >> null
        noExceptionThrown()
    }

    def "Should not fail when checking credentials"() {
        given:
        Credentials credentials = Mock(TokenAuthCredentials)
        bugtrackerConnector.bugtracker = Mock(BugTracker)

        when:
        bugtrackerConnector.checkCredentials(credentials as Credentials)

        then:
        1 * credentials.getImplementedProtocol() >> TOKEN_AUTH
        1 * mantisService.init(bugtrackerConnector.bugtracker, bugtrackerConnector.credentialHolder) >> null
        noExceptionThrown()
    }

    def "Should not fail when setting bugtracker"() {
        given:
        BugTracker bugTracker = Mock()

        when:
        bugtrackerConnector.setBugtracker(bugTracker)

        then:
        noExceptionThrown()
        bugtrackerConnector.bugtracker == bugTracker
    }

    def "Should return bugtracker"() {
        given:
        bugtrackerConnector.bugtracker = Mock(BugTracker)

        when:
        def result = bugtrackerConnector.getBugtracker()

        then:
        noExceptionThrown()
        result == bugtrackerConnector.bugtracker
    }
}
