/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.service


import org.springframework.context.MessageSource
import org.springframework.web.client.HttpClientErrorException
import org.squashtest.csp.core.bugtracker.core.BugTrackerRemoteException
import org.squashtest.csp.core.bugtracker.domain.BTIssue
import org.squashtest.tm.bugtracker.advanceddomain.AdvancedIssue
import org.squashtest.tm.bugtracker.advanceddomain.AdvancedProject
import org.squashtest.tm.bugtracker.advanceddomain.FieldValue
import org.squashtest.tm.bugtracker.advanceddomain.RemoteIssueSearchRequest
import org.squashtest.tm.bugtracker.advanceddomain.RemoteIssueSearchTerm
import org.squashtest.tm.bugtracker.definition.Attachment
import org.squashtest.tm.bugtracker.definition.RemoteIssue
import org.squashtest.tm.bugtracker.definition.context.ExecutionInfo
import org.squashtest.tm.bugtracker.definition.context.ExecutionStepInfo
import org.squashtest.tm.bugtracker.definition.context.RemoteIssueContext
import org.squashtest.tm.bugtracker.definition.context.TestCaseInfo
import org.squashtest.tm.core.foundation.exception.NullArgumentException
import org.squashtest.tm.domain.bugtracker.BugTracker
import org.squashtest.tm.domain.servers.TokenAuthCredentials
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.CredentialHolder
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.MantisClientImpl
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.exception.ExceptionHandler
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Issue
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Project
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.ProjectCustomField
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Severity
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Status
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.User
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.response.MantisIssueResponse
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.response.MantisIssuesResponse
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.response.MantisProjectsResponse
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.project.customfields.SchemeBuilder
import spock.lang.Specification

class MantisServiceImplTest extends Specification {
    private MantisClientImpl mantisClient
    private MantisService mantisService
    private SchemeBuilder schemeBuilder
    private MantisProjectService projectService
    private MessageSource messageSource
    private CredentialHolder credentialHolder
    private TokenAuthCredentials tokenAuthCredentials

    def setup() {
        mantisClient = Mock()
        projectService = Mock()
        messageSource = Mock()
        schemeBuilder = Mock()
        mantisService = new MantisServiceImpl(mantisClient, new ExceptionHandler(messageSource),
                schemeBuilder, projectService, messageSource)
        credentialHolder = Mock()
        tokenAuthCredentials = new TokenAuthCredentials("test")
    }

    def "Should not fail when initializing mantis service"() {
        given:
        BugTracker bugtracker = Mock()
        credentialHolder.getCredentials() >> tokenAuthCredentials

        when:
        this.mantisService.init(bugtracker, credentialHolder)

        then:
        noExceptionThrown()
        1 * mantisClient.init(bugtracker, credentialHolder)
    }

    def "Should fail when initializing mantis service with empty token"() {
        given:
        BugTracker bugtracker = Mock()
        tokenAuthCredentials = new TokenAuthCredentials("")
        credentialHolder.getCredentials() >> tokenAuthCredentials

        when:
        this.mantisService.init(bugtracker, credentialHolder)

        then:
        thrown(NullArgumentException)
        0 * mantisClient.init(bugtracker, credentialHolder)
    }

    def "Should fail when initializing mantis service with null credentials holder"() {
        given:
        BugTracker bugtracker = Mock()
        credentialHolder = null

        when:
        this.mantisService.init(bugtracker, credentialHolder)

        then:
        thrown(NullArgumentException)
        0 * mantisClient.init(bugtracker, credentialHolder)
    }

    def "should not fail when forward attachments"(){
        given:
        File tmp = File.createTempFile("text", ".txt")
        InputStream stream = new FileInputStream(tmp)
        List<Attachment> attachments = Arrays.asList(
                new Attachment(tmp.name, 5, stream)
        )
        credentialHolder.getCredentials() >> tokenAuthCredentials

        when:
        mantisService.forwardAttachments("1", attachments, credentialHolder)

        then:
        1 * mantisClient.uploadFiles(_,_,_)
        noExceptionThrown()
    }

    def "should fail when forward attachments"(){
        given:
        InputStream stream = null
        List<Attachment> attachments = Arrays.asList(
                new Attachment("test.test,", 5, stream)
        )
        credentialHolder.getCredentials() >> tokenAuthCredentials

        when:
        mantisService.forwardAttachments("1", attachments, credentialHolder)

        then:
        thrown(BugTrackerRemoteException)
    }

    def "Should return known issues"() {
        given:
        credentialHolder.getCredentials() >> tokenAuthCredentials
        List<String> issueKeys = new ArrayList<>(Arrays.asList("1", "2", "3"))
        Issue issue = new Issue("1")
        issue.setSummary("issue 1")
        List<Issue> issues = new ArrayList<>(Arrays.asList(issue))
        MantisIssuesResponse issuesResponse = new MantisIssuesResponse()
        issuesResponse.setIssues(issues)
        MantisProjectsResponse projectsResponse = new MantisProjectsResponse()
        Project project = new Project("1", "test")
        List<Project> projects = new ArrayList<>(Arrays.asList(project))
        projectsResponse.setProjects(projects)
        issue.setProject(project)
        issue.setSeverity(new Severity())
        issue.setStatus(new Status())

        when:
        def result = mantisService.findKnownIssues(issueKeys, credentialHolder)

        then:
        3 * mantisClient.findIssues(_, credentialHolder) >> issuesResponse
        3 * projectService.searchProjectById(_, credentialHolder, mantisClient) >> _
        result.size() == 3
        noExceptionThrown()
    }

    def "Should return no issues"() {
        given:
        credentialHolder.getCredentials() >> tokenAuthCredentials
        List<String> issueKeys = new ArrayList<>()

        when:
        def result = mantisService.findKnownIssues(issueKeys, credentialHolder)

        then:
        result.size() == 0
        noExceptionThrown()
    }

    def "Should not fail while searching for unknown issues"() {
        given:
        credentialHolder.getCredentials() >> tokenAuthCredentials
        Exception e = GroovyMock(HttpClientErrorException.NotFound)
        List<String> issueKeys = new ArrayList<>(Arrays.asList("1"))
        List<Issue> issues = new ArrayList<>()
        MantisIssuesResponse issuesResponse = new MantisIssuesResponse()
        issuesResponse.setIssues(issues)
        MantisProjectsResponse projectsResponse = new MantisProjectsResponse()
        Project project = new Project("1", "test")
        List<Project> projects = new ArrayList<>(Arrays.asList(project))
        projectsResponse.setProjects(projects)

        when:
        def result = mantisService.findKnownIssues(issueKeys, credentialHolder)

        then:
        1 * mantisClient.findIssues(_, credentialHolder) >> {throw e }
        noExceptionThrown()
        !result.isEmpty()
    }

    def "Should return an optional issue"() {
        given:
        credentialHolder.getCredentials() >> tokenAuthCredentials
        List<RemoteIssueSearchTerm> searchTerms = new ArrayList<>(Arrays.asList(new RemoteIssueSearchTerm("key", "1")))
        RemoteIssueSearchRequest remoteIssueSearchRequest = new RemoteIssueSearchRequest(searchTerms)
        Issue issue = new Issue("1")
        issue.setSummary("issue 1")
        List<Issue> issues = new ArrayList<>(Arrays.asList(issue))
        MantisIssuesResponse issuesResponse = new MantisIssuesResponse()
        issuesResponse.setIssues(issues)
        MantisProjectsResponse projectsResponse = new MantisProjectsResponse()
        Project project = new Project("1", "test")
        List<Project> projects = new ArrayList<>(Arrays.asList(project))
        projectsResponse.setProjects(projects)
        issue.setProject(project)
        issue.setSeverity(new Severity())
        issue.setStatus(new Status())

        when:
        mantisService.searchIssue(remoteIssueSearchRequest, credentialHolder)

        then:
        1 * mantisClient.findIssues(_, credentialHolder) >> issuesResponse
        1 * projectService.searchProjectById(_, credentialHolder, mantisClient) >> _
        noExceptionThrown()
    }

    def "Should fail when searching an issue with wrong issue id"() {
        given:
        credentialHolder.getCredentials() >> tokenAuthCredentials
        List<RemoteIssueSearchTerm> searchTerms = new ArrayList<>(Arrays.asList(new RemoteIssueSearchTerm("1", "")))
        RemoteIssueSearchRequest remoteIssueSearchRequest = new RemoteIssueSearchRequest(searchTerms)

        when:
        mantisService.searchIssue(remoteIssueSearchRequest, credentialHolder)

        then:
        thrown(IllegalArgumentException)
    }

    def "Should return no issues when searching an issue with no issue name"() {
        given:
        credentialHolder.getCredentials() >> tokenAuthCredentials
        List<RemoteIssueSearchTerm> searchTerms = new ArrayList<>(Arrays.asList(new RemoteIssueSearchTerm("key", "")))
        RemoteIssueSearchRequest remoteIssueSearchRequest = new RemoteIssueSearchRequest(searchTerms)
        List<Issue> issues = new ArrayList<>()
        MantisIssuesResponse response = new MantisIssuesResponse()
        response.setIssues(issues)
        when:
        mantisService.searchIssue(remoteIssueSearchRequest, credentialHolder)

        then:
        thrown(BugTrackerRemoteException)
    }

    def "Should return remote issue"() {
        given:
        BugTracker bugtracker = Mock()
        credentialHolder.getCredentials() >> tokenAuthCredentials
        RemoteIssueContext context = new RemoteIssueContext(_ as List<ExecutionStepInfo>, 1L, Mock(TestCaseInfo), Mock(ExecutionInfo), "description")
        Project project = new Project("1", "project")
        AdvancedProject advancedProject = new AdvancedProject()
        advancedProject.setName(project.getName())
        advancedProject.setId(project.getId())
        List<ProjectCustomField> customFields = new ArrayList<>()
        project.setCustomFields(customFields)

        when:
        def result = mantisService.createReportIssueTemplate("project", context, credentialHolder, bugtracker)

        then:
        1 * projectService.findProjectByName(_, credentialHolder, mantisClient) >> project
        1 * projectService.convertProjectToAdvancedProject(_, credentialHolder, mantisClient) >> advancedProject
        result.getProject().getName() == "project"
        noExceptionThrown()
        result in RemoteIssue
    }

    def "Should return an advanced issue"() {
        given:
        credentialHolder.getCredentials() >> tokenAuthCredentials
        RemoteIssue remoteIssue = new AdvancedIssue()
        Project project = new Project("1", "project")
        project.setCustomFields(new ArrayList<>())
        AdvancedProject advancedProject = new AdvancedProject()
        advancedProject.setName(project.getName())
        advancedProject.setId(project.getId())
        remoteIssue.setProject(advancedProject)
        Issue issue = new Issue("1")
        issue.setSummary("issue 1")
        remoteIssue.setFieldValue("category", new FieldValue())
        remoteIssue.setFieldValue("severity", new FieldValue())
        remoteIssue.setFieldValue("assignee", new FieldValue())
        remoteIssue.setFieldValue("summary", new FieldValue())
        remoteIssue.setFieldValue("description", new FieldValue())
        remoteIssue.setFieldValue("additionalInformation", new FieldValue())
        remoteIssue.setFieldValue("version", new FieldValue())
        remoteIssue.getFieldValue("version").setId("none")
        remoteIssue.setFieldValue("priority", new FieldValue())
        remoteIssue.setFieldValue("reproducibility", new FieldValue())
        remoteIssue.setFieldValue("stepsToReproduce", new FieldValue())
        remoteIssue.setFieldValue("viewState", new FieldValue())
        remoteIssue.getFieldValue("viewState").setScalar("true")
        remoteIssue.setFieldValue("tags", new FieldValue())
        issue.setSeverity(new Severity("1", "test"))
        issue.setStatus(new Status("1", "test"))

        when:
        def result = mantisService.createIssue(remoteIssue, credentialHolder)

        then:
        1 * projectService.findProjectById(_, credentialHolder, mantisClient) >> project
        1 * mantisClient.createIssue(_, credentialHolder) >> new MantisIssueResponse(issue)
        result in AdvancedIssue
        noExceptionThrown()
    }

    def "Should return an advanced issue when issue has handler"() {
        given:
        credentialHolder.getCredentials() >> tokenAuthCredentials
        RemoteIssue remoteIssue = new AdvancedIssue()
        Project project = new Project("1", "project")
        project.setCustomFields(new ArrayList<>())
        AdvancedProject advancedProject = new AdvancedProject()
        advancedProject.setName(project.getName())
        advancedProject.setId(project.getId())
        remoteIssue.setProject(advancedProject)
        Issue issue = new Issue("1")
        issue.setSummary("issue 1")
        issue.setHandler(new User("name"))
        remoteIssue.setFieldValue("category", new FieldValue())
        remoteIssue.setFieldValue("severity", new FieldValue())
        remoteIssue.setFieldValue("assignee", new FieldValue())
        remoteIssue.setFieldValue("summary", new FieldValue())
        remoteIssue.setFieldValue("description", new FieldValue())
        remoteIssue.setFieldValue("additionalInformation", new FieldValue())
        remoteIssue.setFieldValue("version", new FieldValue())
        remoteIssue.getFieldValue("version").setId("none")
        remoteIssue.setFieldValue("priority", new FieldValue())
        remoteIssue.setFieldValue("reproducibility", new FieldValue())
        remoteIssue.setFieldValue("stepsToReproduce", new FieldValue())
        remoteIssue.setFieldValue("viewState", new FieldValue())
        remoteIssue.getFieldValue("viewState").setScalar("true")
        remoteIssue.setFieldValue("tags", new FieldValue())
        issue.setSeverity(new Severity("1", "test"))
        issue.setStatus(new Status("1", "test"))

        when:
        def result = mantisService.createIssue(remoteIssue, credentialHolder)

        then:
        1 * projectService.findProjectById(_, credentialHolder, mantisClient) >> project
        1 * mantisClient.createIssue(_, credentialHolder) >> new MantisIssueResponse(issue)
        result in AdvancedIssue
        noExceptionThrown()
    }

    def "Should fail when creating issue if remote issue is not instance of advanced issue"() {
        given:
        credentialHolder.getCredentials() >> tokenAuthCredentials
        BTIssue issue = Mock()

        when:
        mantisService.createIssue(issue, credentialHolder)

        then:
        thrown(RuntimeException)
    }
}
