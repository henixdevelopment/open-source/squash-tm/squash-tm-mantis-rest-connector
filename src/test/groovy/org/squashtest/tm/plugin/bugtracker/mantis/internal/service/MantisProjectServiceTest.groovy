/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.service

import org.springframework.context.MessageSource
import org.squashtest.csp.core.bugtracker.core.BugTrackerRemoteException
import org.squashtest.tm.bugtracker.advanceddomain.AdvancedProject
import org.squashtest.tm.bugtracker.advanceddomain.Field
import org.squashtest.tm.domain.servers.TokenAuthCredentials
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.CredentialHolder
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.MantisClient
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.MantisClientImpl
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.exception.ExceptionHandler
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.MantisConfig
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Priority
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Project
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Reproducibility
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Severity
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.response.MantisConfigsResponse
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.response.MantisProjectsResponse
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.project.customfields.SchemeBuilder
import spock.lang.Specification

class MantisProjectServiceTest extends Specification {
    private MantisClientImpl mantisClient
    private MessageSource messageSource
    private CredentialHolder credentialHolder
    private TokenAuthCredentials tokenAuthCredentials
    private SchemeBuilder schemeBuilder
    private MantisProjectService mantisProjectService
    private List<Severity> severities
    private List<Priority> priorities
    private List<Reproducibility> reproducibilities

    def setup() {
        mantisClient = Mock()
        messageSource = Mock()
        credentialHolder = Mock()
        tokenAuthCredentials = new TokenAuthCredentials("test")
        schemeBuilder = Mock()
        mantisProjectService = new MantisProjectService(schemeBuilder, new ExceptionHandler(messageSource))
        severities = new ArrayList<>(Arrays.asList(new Severity("1", "test")))
        priorities = new ArrayList<>(Arrays.asList(new Priority("test")))
        reproducibilities = new ArrayList<>(Arrays.asList(new Reproducibility("test")))
    }

    def "should return an advanced project"() {
        given:
        credentialHolder.getCredentials() >> tokenAuthCredentials
        Project project = new Project("1", "test")
        MantisProjectsResponse mantisProjectsResponse = new MantisProjectsResponse()
        List<Project> projects = new ArrayList<>(Arrays.asList(project))
        mantisProjectsResponse.setProjects(projects)

        when:
        def result = mantisProjectService.searchProjectById("1" as String, credentialHolder, mantisClient as MantisClient)

        then:
        1 * mantisClient.findProjectById("1" as String, credentialHolder) >> mantisProjectsResponse
        1 * mantisClient.getSeverities(_ as CredentialHolder) >> new MantisConfigsResponse(new ArrayList<MantisConfig>(Arrays.asList(new MantisConfig(severities))))
        1 * mantisClient.getPriorities(_ as CredentialHolder) >> new MantisConfigsResponse(new ArrayList<MantisConfig>(Arrays.asList(new MantisConfig(priorities))))
        1 * mantisClient.getReproducibilities(_ as CredentialHolder) >> new MantisConfigsResponse(new ArrayList<MantisConfig>(Arrays.asList(new MantisConfig(reproducibilities))))
        1 * schemeBuilder.buildSchemes(project, _, _, _) >> new HashMap<String, List<Field>>()
        result in AdvancedProject
        noExceptionThrown()
    }

    def "should fail when searching project by id"() {
        given:
        credentialHolder.getCredentials() >> tokenAuthCredentials
        MantisProjectsResponse mantisProjectsResponse = new MantisProjectsResponse()
        List<Project> projects = new ArrayList<>()
        mantisProjectsResponse.setProjects(projects)

        when:
        mantisProjectService.findProjectById("1" as String, credentialHolder, mantisClient as MantisClient)

        then:
        1 * mantisClient.findProjectById("1" as String, credentialHolder) >> mantisProjectsResponse
        thrown(BugTrackerRemoteException)
    }

    def "should find project by name"() {
        given:
        credentialHolder.getCredentials() >> tokenAuthCredentials
        Project project1 = new Project("1", "test")
        Project project2 = new Project("2", "test2")
        MantisProjectsResponse mantisProjectsResponse = new MantisProjectsResponse()
        List<Project> projects = new ArrayList<>(Arrays.asList(project1, project2))
        mantisProjectsResponse.setProjects(projects)

        when:
        def result = mantisProjectService.findProjectByName("test2" as String, credentialHolder, mantisClient as MantisClient)

        then:
        1 * mantisClient.findAllProjects(credentialHolder) >> mantisProjectsResponse
        result in Project
        noExceptionThrown()
    }

    def "should fail when finding project by name"() {
        given:
        credentialHolder.getCredentials() >> tokenAuthCredentials
        Project project1 = new Project("1", "test")
        MantisProjectsResponse mantisProjectsResponse = new MantisProjectsResponse()
        List<Project> projects = new ArrayList<>(Arrays.asList(project1))
        mantisProjectsResponse.setProjects(projects)

        when:
        mantisProjectService.findProjectByName("test2" as String, credentialHolder, mantisClient as MantisClient)

        then:
        1 * mantisClient.findAllProjects(credentialHolder) >> mantisProjectsResponse
        thrown(BugTrackerRemoteException)
    }
}
