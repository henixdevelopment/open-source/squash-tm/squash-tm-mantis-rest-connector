#
# This file is part of the Squashtest platform.
# Copyright (C) 2022 Henix, henix.fr
#
# See the NOTICE file distributed with this work for additional
# information regarding copyright ownership.
#
# This is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# this software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this software.  If not, see <http://www.gnu.org/licenses/>.
#

bugtracker.mantis.interface.report.assignee.label            = Asignado
bugtracker.mantis.interface.report.category.label            = Categor\u00EDa
bugtracker.mantis.interface.report.comment.label             = Informaciones adicionales
bugtracker.mantis.interface.report.description.label         = Descripci\u00F3n
bugtracker.mantis.interface.report.lists.emptyassignee.label = -- no asignable --
bugtracker.mantis.interface.report.lists.emptycategory.label = -- ninguna --
# contenido para listas vacías
bugtracker.mantis.interface.report.lists.emptyversion.label  = -- ninguna --
bugtracker.mantis.interface.report.severity.label            = Severidad
######## Interfaz ##################
# sí, la prioridad corresponde a las severidades mantis
# Etiquetas para la ventana emergente
bugtracker.mantis.interface.report.summary.label             = Resumen
bugtracker.mantis.interface.report.version.label             = Versi\u00F3n del producto
bugtracker.mantis.interface.table.assignee.header            = Asignado
bugtracker.mantis.interface.table.bug-in-error               = Se elimin\u00F3 la anomal\u00EDa.
bugtracker.mantis.interface.table.description.header         = Descripci\u00F3n
# encabezamientos de tabla
bugtracker.mantis.interface.table.issueid.header             = ID
# valores por defecto para los valores que faltan
bugtracker.mantis.interface.table.null.assignee.label        = ningun
bugtracker.mantis.interface.table.severity.header            = Severidad
bugtracker.mantis.interface.table.reportedin.header          = Reportada en
bugtracker.mantis.interface.table.status.header              = Estado
bugtracker.mantis.interface.table.summary.header             = Resumen

bugtracker.mantis.interface.report.title.label               = Title
bugtracker.mantis.interface.report.iteration.label           = Iteration
bugtracker.mantis.interface.report.attachments.label         = Attachments
bugtracker.mantis.interface.report.tags.label                = Tags
bugtracker.mantis.interface.report.visibility.label          = Visibilidad
bugtracker.mantis.interface.report.steps-to-reproduce.label  = Pasos para reproducir
bugtracker.mantis.interface.report.reproducibility.label     = Reproducibilidad
bugtracker.mantis.interface.report.priority.label            = Prioridad
bugtracker.mantis.interface.report.template.label            = Template

bugtracker.mantis.interface.report.severity.value.feature     = funcionalidad
bugtracker.mantis.interface.report.severity.value.trivial     = trivial
bugtracker.mantis.interface.report.severity.value.text        = texto
bugtracker.mantis.interface.report.severity.value.tweak       = ajuste
bugtracker.mantis.interface.report.severity.value.minor       = menor
bugtracker.mantis.interface.report.severity.value.major       = mayor
bugtracker.mantis.interface.report.severity.value.crash       = fallo
bugtracker.mantis.interface.report.severity.value.block       = bloqueo

bugtracker.mantis.interface.report.reproducibility.value.have-not-tried = no se ha intentado
bugtracker.mantis.interface.report.reproducibility.value.unable-to-duplicate = no reproducible
bugtracker.mantis.interface.report.reproducibility.value.random      = aleatorio
bugtracker.mantis.interface.report.reproducibility.value.always      = siempre
bugtracker.mantis.interface.report.reproducibility.value.sometimes   = a veces
bugtracker.mantis.interface.report.reproducibility.value.N/A         = desconocido

bugtracker.mantis.interface.report.priority.value.none               = ninguna
bugtracker.mantis.interface.report.priority.value.low                = baja
bugtracker.mantis.interface.report.priority.value.normal             = normal
bugtracker.mantis.interface.report.priority.value.high               = alta
bugtracker.mantis.interface.report.priority.value.urgent             = urgente
bugtracker.mantis.interface.report.priority.value.immediate          = immediata

bugtracker.mantis.interface.report.option.none               = None
bugtracker.mantis.interface.report.option.default            = Default

bugtracker.mantis.interface.report.confirm-template-change   = Changing template will set all fields to their value defined in the template. All modifications will be lost.

bugtracker.mantis.interface.exception.unknown-exception      = Se ha producido un error :
bugtracker.mantis.interface.exception.not-found.project      = No se ha podido encontrar el proyecto actual ''{0}'' en el bugtracker. Por favor, contacte el equipo de administraci\u00F3n para informarles que la configuraci\u00F3n es incorrecta. Los nombres de los proyectos en Squash y en el bugtracker deben ser estrictamente los mismos (cuidado, en particular, a los espacios excedentarios al inicio y al fin del nombre del proyecto).
bugtracker.mantis.interface.exception.wrong-project-path     = The project path ''{0}'' is invalid. It should take the form of <organization name>/<project name>.
bugtracker.mantis.interface.exception.not-found.issue        = No existe la anomal\u00EDa indicada, o no tiene suficientes permisos para verla
bugtracker.mantis.interface.exception.cant-upload-file       = The file could not upload
bugtracker.mantis.interface.exception.access-denied          = Fallo de conexi\u00F3n, por favor entre sus credenciales de nuevo.
bugtracker.mantis.interface.exception.missing-right          = You do not have write rights in the project, please check the rights defined for the user or for the Personal Access Token.

bugtracker.mantis.interface.attach.project-path.label        = Project Path
bugtracker.mantis.interface.attach.issue-id.label            = Issue Id

bugtracker.mantis.interface.table.missing-issue.label        = This issue was not found
bugtracker.mantis.interface.table.missing-project.label      = This issue's project was not found

bugtracker.mantis.interface.configuration.project-help-message = This should be the project's path as it appears in the bugtracker URL. e.g. <organization name>/<project name>. Multiple projects can be associated.

bugtracker.mantis.interface.table.severity.value.feature     = Functionalidad
bugtracker.mantis.interface.table.severity.value.trivial     = Trivial
bugtracker.mantis.interface.table.severity.value.text        = Texto
bugtracker.mantis.interface.table.severity.value.tweak       = Ajuste
bugtracker.mantis.interface.table.severity.value.minor       = Menor
bugtracker.mantis.interface.table.severity.value.major       = Mayor
bugtracker.mantis.interface.table.severity.value.crash       = Fallo
bugtracker.mantis.interface.table.severity.value.block       = Bloqueo

bugtracker.mantis.interface.table.status.value.assigned      = asignado
bugtracker.mantis.interface.table.status.value.new           = nuevo
bugtracker.mantis.interface.table.status.value.resolved      = resuelto
bugtracker.mantis.interface.table.status.value.feedback      = retroalimentaci\u00F3n
bugtracker.mantis.interface.table.status.value.acknowledged  = admitido
bugtracker.mantis.interface.table.status.value.confirmed     = confirmad
bugtracker.mantis.interface.table.status.value.closed        = cerrado
