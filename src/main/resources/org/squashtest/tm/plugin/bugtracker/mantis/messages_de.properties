#
# This file is part of the Squashtest platform.
# Copyright (C) 2022 Henix, henix.fr
#
# See the NOTICE file distributed with this work for additional
# information regarding copyright ownership.
#
# This is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# this software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this software.  If not, see <http://www.gnu.org/licenses/>.
#

bugtracker.mantis.interface.report.assignee.label            = Zugewiesen an
bugtracker.mantis.interface.report.category.label            = Kategorie
bugtracker.mantis.interface.report.comment.label             = Weitere Informationen
bugtracker.mantis.interface.report.description.label         = Beschreibung
bugtracker.mantis.interface.report.lists.emptyassignee.label = -- nicht zuweisbar --
bugtracker.mantis.interface.report.lists.emptycategory.label = -- keine --
# empty lists contents
bugtracker.mantis.interface.report.lists.emptyversion.label  = -- keine -- 
######## Interface ##################
#yes, the priority maps to Mantis' severity
# popup labels
bugtracker.mantis.interface.report.priority.label            = Priorit\u00E4t
bugtracker.mantis.interface.report.summary.label             = Zusammenfassung
bugtracker.mantis.interface.table.severity.label             = Schwere
bugtracker.mantis.interface.report.version.label             = Produktversion
bugtracker.mantis.interface.table.assignee.header            = Zugewiesen an
bugtracker.mantis.interface.table.bug-in-error               =Der Fehler ist gel\u00F6cht
bugtracker.mantis.interface.table.description.header         = Beschreibung
# table headers
bugtracker.mantis.interface.table.issueid.header             = ID
# table default for null values
bugtracker.mantis.interface.table.null.assignee.label        = keiner
bugtracker.mantis.interface.table.priority.header            = Priorit\u00E4t
bugtracker.mantis.interface.table.reportedin.header          = Gemeldet in
bugtracker.mantis.interface.table.status.header              = Status
bugtracker.mantis.interface.table.summary.header             = Zusammenfassung

bugtracker.mantis.interface.report.title.label               = Title
bugtracker.mantis.interface.report.attachments.label         = Attachments
bugtracker.mantis.interface.report.tags.label                = Tags
bugtracker.mantis.interface.report.steps-to-reproduce.label  = Schritte zum Reproduzieren
bugtracker.mantis.interface.report.reproducibility.label     = Reproduzierbarkeit
bugtracker.mantis.interface.report.template.label            = Template
bugtracker.mantis.interface.report.visibility.label          = Sichtweite

bugtracker.mantis.interface.report.severity.value.feature     = feature-wunsch
bugtracker.mantis.interface.report.severity.value.trivial     = trivial
bugtracker.mantis.interface.report.severity.value.text        = fehler im text
bugtracker.mantis.interface.report.severity.value.tweak       = unsch\u00f6nheit
bugtracker.mantis.interface.report.severity.value.minor       = kleinerer fehler
bugtracker.mantis.interface.report.severity.value.major       = schwerer fehler
bugtracker.mantis.interface.report.severity.value.crash       = absturz
bugtracker.mantis.interface.report.severity.value.block       = blocker

bugtracker.mantis.interface.report.reproducibility.value.have-not-tried = nicht getestet
bugtracker.mantis.interface.report.reproducibility.value.unable-to-duplicate = nicht reproduzierbar
bugtracker.mantis.interface.report.reproducibility.value.random      = zuf\u00e4llig
bugtracker.mantis.interface.report.reproducibility.value.always      = immer
bugtracker.mantis.interface.report.reproducibility.value.sometimes   = manchmal
bugtracker.mantis.interface.report.reproducibility.value.N/A         = N/A

bugtracker.mantis.interface.report.priority.value.none               = keine
bugtracker.mantis.interface.report.priority.value.low                = niedrig
bugtracker.mantis.interface.report.priority.value.normal             = normal
bugtracker.mantis.interface.report.priority.value.high               = hoch
bugtracker.mantis.interface.report.priority.value.urgent             = dringend
bugtracker.mantis.interface.report.priority.value.immediate          = sofort

bugtracker.mantis.interface.report.option.none               = None
bugtracker.mantis.interface.report.option.default            = Default

bugtracker.mantis.interface.report.confirm-template-change   = Changing template will set all fields to their value defined in the template. All modifications will be lost.

bugtracker.mantis.interface.exception.unknown-exception      = Ein Fehler ist aufgetreten : 
bugtracker.mantis.interface.exception.not-found.project      = Kein Projekt mit dem Namen ''{0}'' konnte im Bugtracker gefunden werden. Bitte wenden Sie sich an ihren Administrator und sagen Sie ihnen dass die Einstellungen inkorrekt sind. Der Projektname in Squash TM und im Bugtracker m\u00FCssen identisch sein (Passen Sie insbesondere auf Leerzeichen vor und nach dem Projektnamen).
bugtracker.mantis.interface.exception.wrong-project-path     = The project path ''{0}'' is invalid. It should take the form of <organization name>/<project name>.
bugtracker.mantis.interface.exception.not-found.issue        = Entweder existiert dieser Fehler nicht oder Ihre Zugriffsrechte reichen nicht aus, um ihn anzuzeigen.
bugtracker.mantis.interface.exception.cant-upload-file       = The file could not upload
bugtracker.mantis.interface.exception.access-denied          = Die Verbindung ist fehlgeschlagen : Bitte versuchen Sie erneut, sich anzumelden.
bugtracker.mantis.interface.exception.missing-right          = You do not have write rights in the project, please check the rights defined for the user or for the Personal Access Token.

bugtracker.mantis.interface.attach.project-path.label        = Project Path
bugtracker.mantis.interface.attach.issue-id.label            = Issue Id

bugtracker.mantis.interface.table.missing-issue.label        = This issue was not found
bugtracker.mantis.interface.table.missing-project.label      = This issue's project was not found

bugtracker.mantis.interface.configuration.project-help-message = This should be the project's path as it appears in the bugtracker URL. e.g. <organization name>/<project name>. Multiple projects can be associated.

bugtracker.mantis.interface.table.severity.value.feature     = Feature-Wunsch
bugtracker.mantis.interface.table.severity.value.trivial     = Trivial
bugtracker.mantis.interface.table.severity.value.text        = Fehler im Text
bugtracker.mantis.interface.table.severity.value.tweak       = Unsch\u00F6nheit
bugtracker.mantis.interface.table.severity.value.minor       = Kleinerer Fehler
bugtracker.mantis.interface.table.severity.value.major       = Schwerer Fehler
bugtracker.mantis.interface.table.severity.value.crash       = Absturz
bugtracker.mantis.interface.table.severity.value.block       = Blocker

bugtracker.mantis.interface.table.status.value.assigned      = zugewiesen
bugtracker.mantis.interface.table.status.value.new           = Neu
bugtracker.mantis.interface.table.status.value.resolved      = Aufgel\u00F6st
bugtracker.mantis.interface.table.status.value.feedback      = R\u00FCckmeldung
bugtracker.mantis.interface.table.status.value.acknowledged  = acknowledged
bugtracker.mantis.interface.table.status.value.confirmed     = anerkannt
bugtracker.mantis.interface.table.status.value.closed        = abgeschlossen
