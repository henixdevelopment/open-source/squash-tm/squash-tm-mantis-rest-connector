/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.project.customfields.converter;

import org.squashtest.tm.bugtracker.advanceddomain.Field;
import org.squashtest.tm.bugtracker.advanceddomain.FieldValue;
import org.squashtest.tm.bugtracker.advanceddomain.InputType;
import org.squashtest.tm.bugtracker.advanceddomain.Rendering;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.ProjectCustomField;

import java.util.Arrays;

public abstract class AbstractFieldConverter implements CustomFieldConverter {

    protected Field createBasicField(ProjectCustomField input, String type) {
        Field field = new Field(input.getName(), input.getName());
        Rendering rendering = new Rendering();
        InputType inputType = new InputType(type, InputType.TypeName.UNKNOWN.value);
        rendering.setInputType(inputType);
        field.setRendering(rendering);
        return field;
    }

    protected Field createCollectionCustomField(ProjectCustomField input, String type) {
        final Field valuesField = createBasicField(input, type);
        valuesField.getRendering().getInputType().setFieldSchemeSelector(true);
        FieldValue[] values = Arrays.stream(input.getPossibleValue().split("\\|")).map(
                value -> new FieldValue(value, value)).toArray(FieldValue[]::new);
        valuesField.setPossibleValues(values);
        return valuesField;
    }

    protected void checkRequiredCUF(ProjectCustomField customField, Field field) {
        if (Boolean.TRUE.equals(customField.getRequireReport())) {
            field.getRendering().setRequired(true);
        }
    }

}
