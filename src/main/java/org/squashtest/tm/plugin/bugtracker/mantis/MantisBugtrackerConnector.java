/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.squashtest.csp.core.bugtracker.core.BugTrackerNoCredentialsException;
import org.squashtest.csp.core.bugtracker.core.BugTrackerRemoteException;
import org.squashtest.csp.core.bugtracker.core.ProjectNotFoundException;
import org.squashtest.csp.core.bugtracker.core.UnsupportedAuthenticationModeException;
import org.squashtest.csp.core.bugtracker.spi.BugTrackerInterfaceDescriptor;
import org.squashtest.tm.bugtracker.advanceddomain.AdvancedIssue;
import org.squashtest.tm.bugtracker.advanceddomain.AdvancedProject;
import org.squashtest.tm.bugtracker.advanceddomain.DelegateCommand;
import org.squashtest.tm.bugtracker.advanceddomain.RemoteIssueSearchRequest;
import org.squashtest.tm.bugtracker.definition.Attachment;
import org.squashtest.tm.bugtracker.definition.RemoteIssue;
import org.squashtest.tm.bugtracker.definition.context.RemoteIssueContext;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.domain.servers.AuthenticationProtocol;
import org.squashtest.tm.domain.servers.Credentials;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.CredentialHolder;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.service.MantisService;
import org.squashtest.tm.service.spi.AdvancedBugTrackerConnector;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Optional;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;
import static org.squashtest.tm.domain.servers.AuthenticationProtocol.TOKEN_AUTH;

@Component
@Scope(SCOPE_PROTOTYPE)
public class MantisBugtrackerConnector implements AdvancedBugTrackerConnector {

    private static final String MANTIS_ISSUE_SUFFIX = "/view.php";

    private BugTracker bugtracker;

    private final MantisBugtrackerConnectorInterfaceDescriptor interfaceDescriptor;

    private final MantisService mantisService;

    private final CredentialHolder credentialHolder = new CredentialHolder();

    public MantisBugtrackerConnector(MantisBugtrackerConnectorInterfaceDescriptor interfaceDescriptor,
                                     MantisService mantisService) {
        this.interfaceDescriptor = interfaceDescriptor;
        this.mantisService = mantisService;
    }

    @Override
    public AuthenticationProtocol[] getSupportedAuthProtocols() {
        return new AuthenticationProtocol[] {TOKEN_AUTH};
    }

    @Override
    public URL makeViewIssueUrl(String issueId) {
        URL url;
        try {
            if (bugtracker.getUrl() != null) {
                URL baseUrl = new URL(bugtracker.getUrl());
                String suffix = MANTIS_ISSUE_SUFFIX + "?id=" + issueId;
                url = new URL(baseUrl + suffix);
            } else {
                url = null;
            }

            return url;
        } catch (MalformedURLException mue) {
            throw new BugTrackerRemoteException(mue);
        }
    }

    @Override
    public AdvancedProject findProject(String s) throws ProjectNotFoundException, BugTrackerRemoteException {
        return null;
    }

    @Override
    public AdvancedProject findProjectById(String s) throws ProjectNotFoundException, BugTrackerRemoteException {
        return null;
    }

    @Override
    public AdvancedIssue createIssue(RemoteIssue remoteIssue) throws BugTrackerRemoteException {
       return mantisService.createIssue(remoteIssue, credentialHolder);
    }

    @Override
    public Optional<AdvancedIssue> searchIssue(RemoteIssueSearchRequest searchRequest) {
        return mantisService.searchIssue(searchRequest, credentialHolder);
    }

    @Override
    public AdvancedIssue findIssue(String s) {
        return null;
    }

    @Override
    public RemoteIssue createReportIssueTemplate(String projectName, RemoteIssueContext context) {
        return mantisService.createReportIssueTemplate(projectName, context, credentialHolder, bugtracker);
    }

    @Override
    public List<AdvancedIssue> findIssues(List<String> list) {
        return mantisService.findKnownIssues(list, credentialHolder);
    }

    @Override
    public void forwardAttachments(String issueId, List<Attachment> attachments) {
        mantisService.forwardAttachments(issueId, attachments, credentialHolder);
    }

    @Override
    public Object executeDelegateCommand(DelegateCommand delegateCommand) {
        return null;
    }

    @Override
    public void linkIssues(String s, List<String> list) {
        // NOOP
    }

    @Override
    public BugTrackerInterfaceDescriptor getInterfaceDescriptor() {
        return this.interfaceDescriptor;
    }

    @Override
    public void authenticate(Credentials credentials) throws UnsupportedAuthenticationModeException {
        credentialHolder.setCredentials(credentials);
        mantisService.init(bugtracker, this.credentialHolder);
    }

    @Override
    public void checkCredentials(Credentials credentials)
            throws BugTrackerNoCredentialsException, BugTrackerRemoteException {
        authenticate(credentials);
    }

    public void setBugtracker(BugTracker bugtracker) {
        this.bugtracker = bugtracker;
    }

    public BugTracker getBugtracker() {
        return this.bugtracker;
    }

}
