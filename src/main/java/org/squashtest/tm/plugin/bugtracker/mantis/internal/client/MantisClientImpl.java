/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.squashtest.csp.core.bugtracker.core.BugTrackerNoCredentialsDetailedException;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.*;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.exception.ExceptionHandler;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.response.MantisConfigsResponse;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.response.MantisIssueResponse;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.response.MantisIssuesResponse;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.response.MantisProjectsResponse;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.utils.RestTemplateFactory;

import java.lang.reflect.Type;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;

@Component
@Scope(SCOPE_PROTOTYPE)
public class MantisClientImpl implements MantisClient {

    private RestTemplate restTemplate;

    private final RestTemplateFactory restTemplateFactory;

    private final ExceptionHandler exceptionHandler;

    private static final Logger LOGGER = LoggerFactory.getLogger(MantisClientImpl.class);

    public MantisClientImpl(RestTemplateFactory restTemplateFactory, ExceptionHandler exceptionHandler) {
        this.restTemplateFactory = restTemplateFactory;
        this.exceptionHandler = exceptionHandler;
    }

    @Override
    public void init(BugTracker bugTracker, CredentialHolder credentialHolder) {
        restTemplate = restTemplateFactory.restTemplate(bugTracker);
        verifyCredentialsValidity(credentialHolder);
    }

    @Override
    public MantisIssuesResponse findIssues(Long issueId, CredentialHolder credentialHolder) {
        String path = PathBuilder.buildIssuePath(issueId.toString()).build();
        try {
            return get(path, MantisIssuesResponse.class, credentialHolder.getCredentials().getToken());
        } catch (HttpClientErrorException.Forbidden |  HttpClientErrorException.NotFound e) {
            LOGGER.error("Forbidden connection, access denied for user!", e);
            throw exceptionHandler.issueNotFound();
        }
    }

    @Override
    public MantisProjectsResponse findProjectById(String id, CredentialHolder credentialHolder) {
        String path = PathBuilder.buildProjectPath(id).build();
        try {
            return get(path, MantisProjectsResponse.class, credentialHolder.getCredentials().getToken());
        } catch (HttpClientErrorException.Forbidden |HttpClientErrorException.NotFound e) {
            LOGGER.error("Forbidden connection, access denied for user!", e);
            throw exceptionHandler.projectNotFound(id);
        }
    }

    @Override
    public MantisProjectsResponse findAllProjects(CredentialHolder credentialHolder) {
        String path = PathBuilder.buildAllProjectsPath().build();
        return getMantisEntity(credentialHolder, MantisProjectsResponse.class, path);
    }

    @Override
    public MantisConfigsResponse getSeverities(CredentialHolder credentialHolder) {
        String path = PathBuilder.buildSeveritiesPath().build();
        return getMantisEntity(credentialHolder, MantisConfigsResponse.class, path);
    }

    @Override
    public MantisConfigsResponse getPriorities(CredentialHolder credentialHolder) {
        String path = PathBuilder.buildPrioritiesPath().build();
        return getMantisEntity(credentialHolder, MantisConfigsResponse.class, path);
    }

    @Override
    public MantisConfigsResponse getReproducibilities(CredentialHolder credentialHolder) {
        String path =PathBuilder.buildReproducibilityPath().build();
        return getMantisEntity(credentialHolder, MantisConfigsResponse.class, path);
    }

    private void verifyCredentialsValidity(CredentialHolder credentialHolder) {
        String path = PathBuilder.buildCurrentUserPath().build();
        getMantisEntity(credentialHolder, User.class, path);
    }

    private <T> T getMantisEntity(CredentialHolder credentialHolder, Type type, String path) {
        try {
            return get(path, type, credentialHolder.getCredentials().getToken());
        } catch (HttpClientErrorException.Forbidden | HttpClientErrorException.NotFound e) {
            throw logAndThrowConnectionDeniedException(e);
        }
    }

    private <T> T get(String path, Type type, String token) throws RestClientException {
        final ParameterizedTypeReference<T> parameterizedTypeReference = ParameterizedTypeReference.forType(type);
        return sendRequest(path, parameterizedTypeReference, token, HttpMethod.GET, null);
    }

    @Override
    public MantisIssueResponse createIssue(Issue issue, CredentialHolder credentialHolder) {
        String path = PathBuilder.buildAddIssuePath().build();
        try {
            return post(path, MantisIssueResponse.class, credentialHolder.getCredentials().getToken(), issue);
        } catch (HttpClientErrorException.Forbidden | HttpClientErrorException.NotFound e) {
            throw logAndThrowConnectionDeniedException(e);
        }
    }

    @Override
    public MantisIssuesResponse uploadFiles(String id, MantisFiles files, CredentialHolder credentialHolder) {
        String path = PathBuilder.buildAttachmentsPath(id).build();
        try {
            return post(path, MantisIssuesResponse.class, credentialHolder.getCredentials().getToken(), files);
        } catch (HttpClientErrorException.Forbidden | HttpClientErrorException.NotFound e) {
            throw logAndThrowConnectionDeniedException(e);
        }
    }

    private BugTrackerNoCredentialsDetailedException logAndThrowConnectionDeniedException(HttpClientErrorException e) {
        LOGGER.error("Forbidden connection, token is not correct!", e);
        return exceptionHandler.connexionDenied();
    }

    private <T> T post(String path, Type type, String token, Object body) throws HttpClientErrorException.Forbidden, HttpClientErrorException.NotFound {
        final ParameterizedTypeReference<T> parameterizedTypeReference = ParameterizedTypeReference.forType(type);
        return sendRequest(path, parameterizedTypeReference,token, HttpMethod.POST, body);
    }

    private <T> T sendRequest(String path, ParameterizedTypeReference<T> parameterizedTypeReference, String token, HttpMethod method, @Nullable Object body)
            throws HttpClientErrorException.Forbidden, HttpClientErrorException.NotFound {
        final HttpEntity<?> entity = body == null ? getHttpEntity(token) : getHttpEntityWithBody(token, body);

        try {
            final ResponseEntity<T> responseEntity = restTemplate.exchange(path, method, entity, parameterizedTypeReference);
            return responseEntity.getBody();
        } catch (HttpClientErrorException.Forbidden | HttpClientErrorException.NotFound e) {
            throw e;
        } catch (HttpClientErrorException e) {
            LOGGER.error("Forbidden connection");
            throw exceptionHandler.mantisHttpError(e);
        } catch (Exception e) {
            throw exceptionHandler.genericError(e);
        }
    }

    private HttpEntity<?> getHttpEntity(String token) {
        HttpHeaders headers = getHttpHeaders(token);
        return new HttpEntity<>(headers);
    }

    private HttpEntity<?> getHttpEntityWithBody(String token, Object body) {
        HttpHeaders headers = getHttpHeaders(token);
        return new HttpEntity<>(body, headers);
    }

    private HttpHeaders getHttpHeaders(String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        headers.set(HttpHeaders.AUTHORIZATION, token);
        return headers;
    }

}
