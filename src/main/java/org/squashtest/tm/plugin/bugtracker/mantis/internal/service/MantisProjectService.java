/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.service;

import org.springframework.stereotype.Service;
import org.squashtest.tm.bugtracker.advanceddomain.AdvancedProject;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.CredentialHolder;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.MantisClient;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.exception.ExceptionHandler;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Project;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.response.MantisConfigsResponse;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.response.MantisProjectsResponse;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.template.LabelTemplate;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.project.customfields.SchemeBuilder;

import java.util.List;
import java.util.stream.Collectors;

@Service("squash.tm.plugin.bugtracker.mantis.MantisProjectService")
public class MantisProjectService {

    private final SchemeBuilder schemeBuilder;
    private final ExceptionHandler exceptionHandler;

    public MantisProjectService(SchemeBuilder schemeBuilder, ExceptionHandler exceptionHandler) {
        this.schemeBuilder = schemeBuilder;
        this.exceptionHandler = exceptionHandler;
    }

    public AdvancedProject searchProjectById(String id, CredentialHolder credentialHolder, MantisClient mantisClient) {
        Project project = findProjectById(id, credentialHolder, mantisClient);
        return convertProjectToAdvancedProject(project, credentialHolder, mantisClient);
    }

    public Project findProjectById(String id, CredentialHolder credentialHolder, MantisClient mantisClient) {
        final MantisProjectsResponse projects = mantisClient.findProjectById(id, credentialHolder);
        if (projects.getProjects().isEmpty()) {
            throw exceptionHandler.projectNotFound(id);
        }
        return projects.getProjects().get(0);
    }

    public Project findProjectByName(String projectName, CredentialHolder credentialHolder, MantisClient mantisClient) {
        MantisProjectsResponse projects = mantisClient.findAllProjects(credentialHolder);
        List<Project> filteredProjects = projects.getProjects().stream().filter(project -> project.getName().equals(projectName)).collect(Collectors.toList());
        if (filteredProjects.isEmpty()) {
            throw exceptionHandler.projectNotFound(projectName);
        }
        return filteredProjects.get(0);
    }

    public AdvancedProject convertProjectToAdvancedProject(Project project, CredentialHolder credentialHolder, MantisClient mantisClient) {
        List<LabelTemplate> severities = getAllSeverities(credentialHolder, mantisClient);
        List<LabelTemplate> priorities = getAllPriorities(credentialHolder, mantisClient);
        List<LabelTemplate> reproducibilities = getAllReproducibilities(credentialHolder, mantisClient);
        return AdvancedProject.create(project.getId(), project.getName(), schemeBuilder.buildSchemes(project, severities, priorities, reproducibilities));
    }

    private List<LabelTemplate> getAllSeverities(CredentialHolder credentialHolder, MantisClient mantisClient) {
        MantisConfigsResponse mantisConfigsResponse = mantisClient.getSeverities(credentialHolder);
        return mantisConfigsResponse.getFirstMantisConfigValues();
    }

    private List<LabelTemplate> getAllPriorities(CredentialHolder credentialHolder, MantisClient mantisClient) {
        MantisConfigsResponse mantisConfigsResponse = mantisClient.getPriorities(credentialHolder);
        return mantisConfigsResponse.getFirstMantisConfigValues();
    }

    private List<LabelTemplate> getAllReproducibilities(CredentialHolder credentialHolder, MantisClient mantisClient) {
        MantisConfigsResponse mantisConfigsResponse = mantisClient.getReproducibilities(credentialHolder);
        return mantisConfigsResponse.getFirstMantisConfigValues();
    }
}
