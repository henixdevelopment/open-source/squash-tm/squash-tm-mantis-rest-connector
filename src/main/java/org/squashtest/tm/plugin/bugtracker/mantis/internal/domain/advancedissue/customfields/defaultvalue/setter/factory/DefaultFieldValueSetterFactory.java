/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.advancedissue.customfields.defaultvalue.setter.factory;

import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.advancedissue.customfields.defaultvalue.setter.BasicDefaultValueSetter;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.advancedissue.customfields.defaultvalue.setter.CollectionDefaultValueSetter;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.advancedissue.customfields.defaultvalue.setter.DateDefaultValueSetter;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.advancedissue.customfields.defaultvalue.setter.DefaultValueSetter;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.project.customfields.CustomFieldsType;

import java.util.Map;
import java.util.Optional;

import static java.util.Map.entry;

public class DefaultFieldValueSetterFactory {

    private final Map<String, DefaultValueSetter> setters;

    public DefaultFieldValueSetterFactory() {
        this.setters = Map.ofEntries(
                entry(CustomFieldsType.TEXT_FIELD, new BasicDefaultValueSetter()),
                entry(CustomFieldsType.NUMERIC_FIELD, new BasicDefaultValueSetter()),
                entry(CustomFieldsType.FLOAT_FIELD, new BasicDefaultValueSetter()),
                entry(CustomFieldsType.ENUM_FIELD, new BasicDefaultValueSetter()),
                entry(CustomFieldsType.EMAIL_FIELD, new BasicDefaultValueSetter()),
                entry(CustomFieldsType.CHECKBOX_FIELD, new CollectionDefaultValueSetter()),
                entry(CustomFieldsType.LIST_FIELD, new BasicDefaultValueSetter()),
                entry(CustomFieldsType.MULTI_SELECTION_FIELD, new CollectionDefaultValueSetter()),
                entry(CustomFieldsType.DATE_FIELD, new DateDefaultValueSetter()),
                entry(CustomFieldsType.RADIO_FIELD, new BasicDefaultValueSetter()),
                entry(CustomFieldsType.TEXTAREA_FIELD, new BasicDefaultValueSetter())
        );
    }

    public DefaultValueSetter get(String type) {
        return Optional.ofNullable(setters.get(type)).orElseThrow(() -> new IllegalArgumentException(type + " does not exist as a custom field type!"));
    }
}

