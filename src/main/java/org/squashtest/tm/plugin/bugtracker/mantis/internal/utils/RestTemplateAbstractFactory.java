/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.utils;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.squashtest.tm.domain.bugtracker.BugTracker;

import javax.inject.Inject;
import java.util.function.Supplier;

public abstract class RestTemplateAbstractFactory implements RestTemplateFactory, InitializingBean {

    @Inject
    protected RestTemplateBuilder builder;

    protected Supplier<ClientHttpRequestFactory> springRequestFactory;

    @Override
    public RestTemplate restTemplate(BugTracker bugTracker) {
        String baseUrl = bugTracker.getUrl();
        DefaultUriBuilderFactory encodingBuilder = new DefaultUriBuilderFactory();
        encodingBuilder.setEncodingMode(DefaultUriBuilderFactory.EncodingMode.NONE);

        return builder.rootUri(baseUrl).uriTemplateHandler(encodingBuilder).requestFactory(springRequestFactory)
                .build();
    }

    protected abstract void initSpringRequestFactory();

    @Override
    public void afterPropertiesSet() throws Exception {
        initSpringRequestFactory();
    }

}
