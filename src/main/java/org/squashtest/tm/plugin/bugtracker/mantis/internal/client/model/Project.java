/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.template.NameTemplate;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Project extends NameTemplate {

    @JsonProperty
    private Status status;

    @JsonProperty
    private String description;

    @JsonProperty
    private Boolean enabled;

    @JsonProperty
    private ViewState visibility;

    @JsonProperty("access_level")
    private ProjectAccessLevel accessLevel;

    @JsonProperty("custom_fields")
    private List<ProjectCustomField> customFields;

    @JsonProperty
    private List<Version> versions;

    @JsonProperty
    private Project subProject;

    @JsonProperty
    private List<Category> categories;

    public Project() {
    }

    public Project(String id, String name) {
        super.id = id;
        super.name = name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public ViewState getVisibility() {
        return visibility;
    }

    public void setVisibility(ViewState visibility) {
        this.visibility = visibility;
    }

    public ProjectAccessLevel getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(ProjectAccessLevel accessLevel) {
        this.accessLevel = accessLevel;
    }

    public List<ProjectCustomField> getCustomFields() {
        return customFields;
    }

    public void setCustomFields(List<ProjectCustomField> customFields) {
        this.customFields = customFields;
    }

    public List<Version> getVersions() {
        return versions;
    }

    public void setVersions(List<Version> versions) {
        this.versions = versions;
    }

    public Project getSubProject() {
        return subProject;
    }

    public void setSubProject(Project subProject) {
        this.subProject = subProject;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

}
