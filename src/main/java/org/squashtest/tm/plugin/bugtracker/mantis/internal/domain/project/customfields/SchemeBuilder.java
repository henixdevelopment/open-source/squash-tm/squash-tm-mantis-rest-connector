/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.project.customfields;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.squashtest.tm.bugtracker.advanceddomain.Field;
import org.squashtest.tm.bugtracker.advanceddomain.FieldValue;
import org.squashtest.tm.bugtracker.advanceddomain.InputType;
import org.squashtest.tm.bugtracker.advanceddomain.Rendering;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Category;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Project;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.ProjectCustomField;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Version;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.template.LabelTemplate;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.template.NameTemplate;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.FieldIds;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.project.customfields.converter.factory.CustomFieldConverterFactory;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.project.customfields.converter.CustomFieldConverterType;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Component("squash.tm.plugin.bugtracker.mantis.SchemeBuilder")
public class SchemeBuilder {

    private final MessageSource messageSource;

    private static final Logger LOGGER = LoggerFactory.getLogger(SchemeBuilder.class);

    public static final String DEFAULT_SCHEME = "Issue";

    public static final String NONE_OPTION = "none";

    public static final String HAVE_NOT_TRIED_OPTION = "have not tried";

    private final CustomFieldConverterFactory customFieldConverterFactory = new CustomFieldConverterFactory();

    public SchemeBuilder(@Named("mantisConnectorMessageSource") MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public Map<String, List<Field>> buildSchemes(Project project, List<LabelTemplate> severities,
                                                 List<LabelTemplate> priorities,
                                                 List<LabelTemplate> reproducibilities) {
        final Map<String, List<Field>> scheme = new HashMap<>();
        List<Field> fields = new ArrayList<>();
        List<Category> categories = project.getCategories();
        List<Version> versions = project.getVersions();

        fields.add(buildCategoryField(categories));
        fields.add(buildReproducibilityField(reproducibilities));
        fields.add(buildSeveritiesField(severities));
        fields.add(buildPrioritiesField(priorities));
        fields.add(buildVersionsField(versions));
        fields.add(buildAssigneeField());
        fields.add(buildSummaryField());
        fields.add(buildDescriptionField());
        fields.add(buildStepsToReproduceField());
        fields.add(buildAdditionalInformationField());
        fields.add(buildTagsField());
        fields.addAll(getAllFields(project));
        fields.add(buildAttachmentsField());
        fields.add(buildVisibilityField());

        scheme.put(DEFAULT_SCHEME, fields);

        return scheme;

    }

    private Field createBasicField(String fieldId, String label, String type) {
        Field field = new Field(fieldId, label);
        Rendering rendering = new Rendering();
        InputType inputType = new InputType(type, InputType.TypeName.UNKNOWN.value);
        rendering.setInputType(inputType);
        field.setRendering(rendering);
        return field;
    }

    private List<Field> getAllFields(Project project) {
        List<ProjectCustomField> customFields = project.getCustomFields();
        List<Field> fields = new ArrayList<>();
        customFields.forEach(customField -> {
            CustomFieldConverterType converterType = customFieldConverterFactory.get(customField.getType());
            Field converted = converterType.getConverter().convert(customField, converterType.getTypeValue());
            fields.add(converted);
        });
        return fields;
    }

    private Field createDropdownListField(String id, String label) {
        return createBasicField(id, label, InputType.TypeName.DROPDOWN_LIST.value);
    }

    private Field buildCategoryField(List<Category> categories) {
        final String label = messageSource.getMessage("interface.report.category.label", null,
                LocaleContextHolder.getLocale());
        Field field = buildCollectionField(categories, label, FieldIds.CATEGORY_FIELD_ID);
        field.getRendering().setRequired(true);
        return field;
    }

    private Field buildSeveritiesField(List<LabelTemplate> severities) {
        final String label = messageSource.getMessage("interface.report.severity.label", null,
                LocaleContextHolder.getLocale());
        return buildTranslationCollectionField(severities, label, FieldIds.SEVERITY_FIELD_ID);
    }

    private Field buildReproducibilityField(List<LabelTemplate> reproducibilities) {
        final String label = messageSource.getMessage("interface.report.reproducibility.label", null,
                LocaleContextHolder.getLocale());

        Field field = buildTranslationCollectionField(reproducibilities, label, FieldIds.REPRODUCIBILITY_FIELD_ID);
        List<FieldValue> possibleValues = Arrays.stream(field.getPossibleValues()).collect(Collectors.toList());
        FieldValue defaultFieldValue = possibleValues.stream().filter(value -> value.getId().equals(HAVE_NOT_TRIED_OPTION))
                .findAny().orElse(null);
        if (!Objects.isNull(defaultFieldValue)) {
            int defaultValueIndex = possibleValues.indexOf(defaultFieldValue);
            Collections.swap(possibleValues, defaultValueIndex, 0);
            field.setPossibleValues(possibleValues.toArray(new FieldValue[0]));
        }

        return field;
    }

    private Field buildPrioritiesField(List<LabelTemplate> priorities) {
        final String label = messageSource.getMessage("interface.report.priority.label", null,
                LocaleContextHolder.getLocale());
        return buildTranslationCollectionField(priorities, label, FieldIds.PRIORITY_FIELD_ID);
    }

    private Field buildAssigneeField() {
        String label = messageSource.getMessage("interface.report.assignee.label", null,
                LocaleContextHolder.getLocale());
        return generateTextField(FieldIds.ASSIGNEE_FIELD_ID, label);

    }

    private Field buildSummaryField() {
        String label = messageSource.getMessage("interface.report.summary.label", null,
                LocaleContextHolder.getLocale());
        Field field = generateTextField(FieldIds.SUMMARY_FIELD_ID, label);
        field.getRendering().setRequired(true);
        return field;
    }

    private Field buildVersionsField(List<Version> versions) {
        final String label = messageSource.getMessage("interface.report.version.label", null,
                LocaleContextHolder.getLocale());
        Field field = buildCollectionField(versions, label, FieldIds.VERSION_FIELD_ID);
        List<FieldValue> possibleVersions = Arrays.stream(field.getPossibleValues()).collect(Collectors.toList());
        possibleVersions.add(0, addEmptyOption());
        field.setPossibleValues(possibleVersions.toArray(new FieldValue[0]));
        return field;
    }

    private Field buildCollectionField(List<? extends NameTemplate> values, String label, String fieldId) {
        final Field field = createDropdownListField(fieldId, label);
        FieldValue[] possibleValues = values.stream().map(
                value -> new FieldValue(value.getName(), value.getName())).toArray(FieldValue[]::new);
        field.setPossibleValues(possibleValues);
        return field;
    }

    private Field buildTranslationCollectionField(List<? extends LabelTemplate> values, String label, String fieldId) {
        final Field field = createDropdownListField(fieldId, label);
        String code = "interface.report." + fieldId + ".value.";

        FieldValue[] possibleValues = values.stream().map(
                value -> new FieldValue(value.getName(),
                        translateFieldName(code, value.getName().replace(" ", "-"), value.getLabel())
                )).toArray(FieldValue[]::new);
        field.setPossibleValues(possibleValues);
        return field;
    }

    public String translateFieldName(String code, String fieldName, String fieldLabel) {
        try {
            String newLabel = messageSource.getMessage(code + fieldName, null,
                    LocaleContextHolder.getLocale());
            return newLabel;
        } catch (NoSuchMessageException e) {
            LOGGER.debug("Unable to translate : " + fieldName, e);
            return fieldLabel;
        }
    }

    private Field generateTextField(String id, String label) {
        return createBasicField(id, label, InputType.TypeName.TEXT_FIELD.value);
    }

    private Field generateCheckBoxField(String id, String label) {
        return createBasicField(id, label, InputType.TypeName.CHECKBOX.value);
    }

    private Field buildDescriptionField() {
        String label = messageSource.getMessage("interface.report.description.label", null,
                LocaleContextHolder.getLocale());
        Field field = generateAreaField(FieldIds.DESCRIPTION_FIELD_ID, label);
        field.getRendering().setRequired(true);
        return field;
    }

    private Field buildAdditionalInformationField() {
        String label = messageSource.getMessage("interface.report.comment.label", null,
                LocaleContextHolder.getLocale());
        return generateAreaField(FieldIds.ADDITIONAL_INFORMATION_FIELD_ID, label);
    }

    private Field buildStepsToReproduceField() {
        String label = messageSource.getMessage("interface.report.steps-to-reproduce.label", null,
                LocaleContextHolder.getLocale());
        return generateAreaField(FieldIds.STEPS_TO_REPRODUCE_FIELD_ID, label);
    }

    private Field buildVisibilityField() {
        String label = messageSource.getMessage("interface.report.visibility.label", null,
                LocaleContextHolder.getLocale());
        return generateCheckBoxField(FieldIds.VIEW_STATE_FIELD_ID, label);
    }

    private Field generateAreaField(String id, String label) {
        return createBasicField(id, label, InputType.TypeName.TEXT_AREA.value);
    }

    private Field buildTagsField() {
        final String label = messageSource.getMessage("interface.report.tags.label", null,
                LocaleContextHolder.getLocale());
        return createBasicField(FieldIds.TAGS_FIELD_ID, label, InputType.TypeName.TAG_LIST.value);
    }

    private Field buildAttachmentsField() {
        String label = messageSource.getMessage("interface.report.attachments.label", null,
                LocaleContextHolder.getLocale());
        return createBasicField(FieldIds.ATTACHMENTS_FIELD_ID, label, InputType.TypeName.FILE_UPLOAD.value);
    }

    private FieldValue addEmptyOption() {
        final String label = messageSource.getMessage("interface.report.lists.emptyversion.label", null,
                LocaleContextHolder.getLocale());
        return new FieldValue(NONE_OPTION, label);
    }

}
