/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.project.customfields.converter;

public class CustomFieldConverterType {

    private final CustomFieldConverter converter;

    private final String typeValue;

    public CustomFieldConverterType(CustomFieldConverter converter, String typeValue) {
        this.converter = converter;
        this.typeValue = typeValue;
    }

    public CustomFieldConverter getConverter() {
        return converter;
    }

    public String getTypeValue() {
        return typeValue;
    }

}
