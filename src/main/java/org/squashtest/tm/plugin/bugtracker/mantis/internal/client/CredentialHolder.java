/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.client;

import org.squashtest.csp.core.bugtracker.core.UnsupportedAuthenticationModeException;
import org.squashtest.tm.domain.servers.Credentials;
import org.squashtest.tm.domain.servers.TokenAuthCredentials;

import static java.util.Objects.requireNonNull;
import static org.squashtest.tm.domain.servers.AuthenticationProtocol.TOKEN_AUTH;

public class CredentialHolder {
    private TokenAuthCredentials credentials;

    public TokenAuthCredentials getCredentials() {
        return requireNonNull(credentials,
                "Credentials are null. You should probably call your Mantis BT administrator to authenticate(Credential cred) before try to use creds");
    }

    public void setCredentials(Credentials credentials) {
        Credentials candidate = requireNonNull(credentials, "Cannot have null credentials");
        if (TOKEN_AUTH.equals(candidate.getImplementedProtocol())) {
            this.credentials = (TokenAuthCredentials) candidate;
        } else {
            throw new UnsupportedAuthenticationModeException(candidate.getImplementedProtocol().name());
        }
    }
}
