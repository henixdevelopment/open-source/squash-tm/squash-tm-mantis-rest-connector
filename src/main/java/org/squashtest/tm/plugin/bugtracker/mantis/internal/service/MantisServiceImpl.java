/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.service;

import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.squashtest.tm.bugtracker.advanceddomain.AdvancedIssue;
import org.squashtest.tm.bugtracker.advanceddomain.AdvancedIssueReportForm;
import org.squashtest.tm.bugtracker.advanceddomain.AdvancedProject;
import org.squashtest.tm.bugtracker.advanceddomain.FieldValue;
import org.squashtest.tm.bugtracker.advanceddomain.RemoteIssueSearchRequest;
import org.squashtest.tm.bugtracker.definition.Attachment;
import org.squashtest.tm.bugtracker.definition.RemoteIssue;
import org.squashtest.tm.bugtracker.definition.context.RemoteIssueContext;
import org.squashtest.tm.core.foundation.exception.NullArgumentException;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.CredentialHolder;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.MantisClient;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.*;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.exception.ExceptionHandler;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.response.MantisIssueResponse;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.response.MantisIssuesResponse;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.advancedissue.customfields.defaultvalue.setter.DefaultValueSetter;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.advancedissue.customfields.defaultvalue.setter.factory.DefaultFieldValueSetterFactory;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.mantisissue.SquashToMantisIssueConverter;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.FieldIds;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.advancedissue.MantisAdvancedIssue;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.project.customfields.SchemeBuilder;

import javax.inject.Named;
import java.util.*;

import static org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.FieldIds.ASSIGNEE_FIELD_ID;
import static org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.FieldIds.PRIORITY_FIELD_ID;
import static org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.FieldIds.STATUS_FIELD_ID;

@Service("squash.tm.plugin.bugtracker.mantis.MantisServiceImpl")
public class MantisServiceImpl implements MantisService {

    private static final String PRIORITY_VALUE = "interface.table.severity.value.";
    private static final String STATUS_VALUE = "interface.table.status.value.";
    private static final Logger LOGGER = LoggerFactory.getLogger(MantisServiceImpl.class);
    private static final String ISSUE_ID = "key";
    private final MantisClient mantisClient;

    private final SchemeBuilder schemeBuilder;
    private final ExceptionHandler exceptionHandler;
    private final MantisProjectService projectService;
    private final MessageSource messageSource;

    private final DefaultFieldValueSetterFactory factory = new DefaultFieldValueSetterFactory();

    public MantisServiceImpl(MantisClient mantisClient, ExceptionHandler exceptionHandler, SchemeBuilder schemeBuilder,
                             MantisProjectService projectService, @Named("mantisConnectorMessageSource") MessageSource messageSource) {
        this.mantisClient = mantisClient;
        this.schemeBuilder = schemeBuilder;
        this.exceptionHandler = exceptionHandler;
        this.projectService = projectService;
        this.messageSource = messageSource;
    }

    @Override
    public void init(BugTracker bugtracker, CredentialHolder credentialHolder) {
        checkEmptyCredentials(credentialHolder);
        mantisClient.init(bugtracker, credentialHolder);
    }

    @Override
    public List<AdvancedIssue> findKnownIssues(List<String> issueKeys, CredentialHolder credentialHolder) {
        var advancedIssues = new ArrayList<AdvancedIssue>();
        issueKeys.forEach(issueKey -> advancedIssues.add(findSingleIssue(credentialHolder, issueKey)));

        return advancedIssues;
    }

    private AdvancedIssue searchSingleIssue(CredentialHolder credentialHolder, String issueKey) {
        try {
            return searchIssue(credentialHolder, issueKey);
        } catch (Exception e) {

            LOGGER.error("Encountered exception while searching issue", e);
            throw exceptionHandler.issueNotFound(issueKey);
        }
    }

    private AdvancedIssue findSingleIssue(CredentialHolder credentialHolder, String issueKey) {
        try {
            return searchIssue(credentialHolder, issueKey);
        } catch (Exception e) {
            LOGGER.error("Encountered exception while fetching issue", e);
            return unknownIssue(issueKey);
        }
    }

    private AdvancedIssue unknownIssue(String issueKey) {
        AdvancedProject missingProject = new AdvancedProject();
        missingProject.setName("-");

        MantisAdvancedIssue missingIssue = new MantisAdvancedIssue();
        missingIssue.setId(issueKey);
        missingIssue.setProject(missingProject);
        missingIssue.setSummary(messageSource
                .getMessage("interface.table.missing-issue.label", null, LocaleContextHolder.getLocale()));
        missingIssue.setFieldValue("priority", new FieldValue(null, null));
        missingIssue.setFieldValue("assignee", new FieldValue(null, null));

        return missingIssue;
    }

    private AdvancedIssue searchIssue(CredentialHolder credentialHolder, String issueKey) {
        Long issueId = Long.parseLong(issueKey);
        final MantisIssuesResponse issues = mantisClient.findIssues(issueId, credentialHolder);
        if (issues.getIssues().isEmpty()) {
            throw exceptionHandler.issueNotFound(issueKey);
        }
        final Issue issue = issues.getIssues().get(0);

        AdvancedProject advancedProject =
                projectService.searchProjectById(issue.getProject().getId(), credentialHolder, mantisClient);

        return convertIssueToAdvancedIssue(issue, advancedProject);
    }

    @Override
    public Optional<AdvancedIssue> searchIssue(RemoteIssueSearchRequest searchRequest,
                                               CredentialHolder credentialHolder) {
        if (searchRequest.hasSearchTerm(ISSUE_ID)) {
            final String issueId = searchRequest.getSearchTermStringValue(ISSUE_ID);
            return Optional.of(searchSingleIssue(credentialHolder, issueId));
        } else {
            throw new IllegalArgumentException(String.format("The search terms should contain %s.", ISSUE_ID));
        }
    }

    @Override
    public RemoteIssue createReportIssueTemplate(String projectName, RemoteIssueContext context,
                                                 CredentialHolder credentialHolder, BugTracker bugTracker) {
        Project project = projectService.findProjectByName(projectName, credentialHolder, mantisClient);
        AdvancedProject advancedProject = projectService.convertProjectToAdvancedProject(project, credentialHolder, mantisClient);
        AdvancedIssueReportForm advancedIssue = new AdvancedIssueReportForm();
        advancedIssue.setBugtracker(bugTracker.getName());
        advancedIssue.setProject(advancedProject);
        advancedIssue.setCurrentScheme(SchemeBuilder.DEFAULT_SCHEME);
        advancedIssue.setRemoteIssueContext(context);
        appendDefaultValues(project, advancedIssue, context);

        return advancedIssue;
    }

    @Override
    public AdvancedIssue createIssue(RemoteIssue remoteIssue, CredentialHolder credentialHolder) {
        if (!(remoteIssue instanceof AdvancedIssue)) {
            throw new RuntimeException("Expected AdvancedIssue");
        }

        final AdvancedIssue advancedIssue = (AdvancedIssue) remoteIssue;
        Project currentProject = projectService.findProjectById(advancedIssue.getProject().getId(), credentialHolder, mantisClient);
        final Issue requestIssue = SquashToMantisIssueConverter.convert(advancedIssue, currentProject);
        MantisIssueResponse response = mantisClient.createIssue(requestIssue, credentialHolder);
        final Issue responseIssue = response.getIssue();
        return convertIssueToAdvancedIssue(responseIssue, advancedIssue.getProject());
    }

    @Override
    public void forwardAttachments(String issueKey, List<Attachment> attachments, CredentialHolder credentialHolder) {
        List<MantisFile> files = new ArrayList<>();
        try {
            for (Attachment attachment : attachments) {
                byte[] bytes = IOUtils.toByteArray(attachment.getStreamContent());
                String content = Base64.getEncoder().encodeToString(bytes);
                MantisFile file = new MantisFile(attachment.getName(), content);
                files.add(file);
            }
            MantisFiles mantisFiles = new MantisFiles();
            mantisFiles.setFiles(files);
            mantisClient.uploadFiles(issueKey, mantisFiles, credentialHolder);
        } catch (Exception e) {
            throw exceptionHandler.cantUploadFiles();
        }
    }

    private AdvancedIssue convertIssueToAdvancedIssue(Issue issue, AdvancedProject advancedProject) {
        final MantisAdvancedIssue advancedIssue =
                MantisAdvancedIssue.from(createAdvancedIssueReportForm(advancedProject));
        advancedIssue.setId(issue.getId());
        advancedIssue.setSummary(issue.getSummary());
        advancedIssue.setDescription(issue.getDescription());
        setAdvancedIssueFieldValueFromIssue(issue, advancedIssue);

        return advancedIssue;
    }

    private void setAdvancedIssueFieldValueFromIssue(Issue issue, MantisAdvancedIssue advancedIssue) {
        advancedIssue.setFieldValue(ASSIGNEE_FIELD_ID, issue.getHandler() != null ?
                new FieldValue(issue.getHandler().getId(), issue.getHandler().getName()) : new FieldValue("", ""));

        advancedIssue.setFieldValue(PRIORITY_FIELD_ID,
                new FieldValue(issue.getSeverity().getId(),
                        schemeBuilder.translateFieldName(PRIORITY_VALUE, issue.getSeverity().getName(), issue.getSeverity().getLabel())));

        advancedIssue.setFieldValue(STATUS_FIELD_ID,
                new FieldValue(issue.getStatus().getId(),
                        schemeBuilder.translateFieldName(STATUS_VALUE, issue.getStatus().getName(), issue.getStatus().getLabel())));
    }

    private AdvancedIssueReportForm createAdvancedIssueReportForm(AdvancedProject project) {
        AdvancedIssueReportForm advancedIssue = new AdvancedIssueReportForm();
        advancedIssue.setProject(project);
        advancedIssue.setCurrentScheme(SchemeBuilder.DEFAULT_SCHEME);
        return advancedIssue;
    }

    private void checkEmptyCredentials(CredentialHolder credentialHolder) {
        LOGGER.info("Checking if credentials are empty...");
        if (credentialHolder == null) {
            LOGGER.info("Credential holder is empty.");
            throw new NullArgumentException("credentials");
        }

        if (credentialHolder.getCredentials().getToken().isEmpty()) {
            LOGGER.info("Token is empty!!");
            throw new NullArgumentException("token");
        }
        LOGGER.info("Credentials checking is finished successfully");
    }

    private void appendDefaultValues(Project project, AdvancedIssue issue, RemoteIssueContext context) {
        String stepsToReproduceScalar = context != null ? context.getDefaultDescription() : "";

        issue.setFieldValue(FieldIds.STEPS_TO_REPRODUCE_FIELD_ID,
                new FieldValue(null, stepsToReproduceScalar));
        issue.setFieldValue(FieldIds.DESCRIPTION_FIELD_ID,
                new FieldValue(null, ""));

        project.getCustomFields().forEach(field -> {
            DefaultValueSetter setter = factory.get(field.getType());
            setter.setDefaultValue(field, issue);
        });
    }

}
