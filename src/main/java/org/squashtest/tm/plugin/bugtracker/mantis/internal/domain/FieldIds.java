/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.domain;

public final class FieldIds {

    private FieldIds() {
    }

    public static final String SUMMARY_FIELD_ID = "summary";
    public static final String CATEGORY_FIELD_ID = "category";
    public static final String SEVERITY_FIELD_ID = "severity";
    public static final String VERSION_FIELD_ID = "version";
    public static final String DESCRIPTION_FIELD_ID = "description";
    public static final String ADDITIONAL_INFORMATION_FIELD_ID = "additionalInformation";
    public static final String ASSIGNEE_FIELD_ID = "assignee";
    public static final String PRIORITY_FIELD_ID = "priority";
    public static final String STATUS_FIELD_ID = "status";
    public static final String REPRODUCIBILITY_FIELD_ID = "reproducibility";
    public static final String STEPS_TO_REPRODUCE_FIELD_ID = "stepsToReproduce";
    public static final String TAGS_FIELD_ID = "tags";
    public static final String VIEW_STATE_FIELD_ID = "viewState";
    public static final String ATTACHMENTS_FIELD_ID = "attachments";
}
