/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.template.NameTemplate;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectCustomField extends NameTemplate {

    @JsonProperty
    private String type;

    @JsonProperty("default_value")
    private String defaultValue;

    @JsonProperty("possible_values")
    private String possibleValue;

    @JsonProperty("valid_regexp")
    private String validRegex;

    @JsonProperty("length_min")
    private String lengthMin;

    @JsonProperty("length_max")
    private String lengthMax;

    @JsonProperty("access_level_r")
    private AccessLevel accessLevelRead;

    @JsonProperty("access_level_rw")
    private AccessLevel accessLevelWrite;

    @JsonProperty("display_report")
    private Boolean displayReport;

    @JsonProperty("display_update")
    private Boolean displayUpdate;

    @JsonProperty("display_resolved")
    private Boolean displayResolved;

    @JsonProperty("display_closed")
    private Boolean displayClosed;

    @JsonProperty("require_report")
    private Boolean requireReport;

    @JsonProperty("require_update")
    private Boolean requireUpdate;

    @JsonProperty("require_resolved")
    private Boolean requireResolved;

    @JsonProperty("require_closed")
    private Boolean requireClosed;

    public ProjectCustomField() {
    }

    public ProjectCustomField(String type, String name) {
        super.name = name;
        this.type = type;
    }

    public ProjectCustomField(String type, String name, String possibleValue) {
        super.name = name;
        this.type = type;
        this.possibleValue = possibleValue;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getPossibleValue() {
        return possibleValue;
    }

    public void setPossibleValue(String possibleValue) {
        this.possibleValue = possibleValue;
    }

    public String getValidRegex() {
        return validRegex;
    }

    public void setValidRegex(String validRegex) {
        this.validRegex = validRegex;
    }

    public String getLengthMin() {
        return lengthMin;
    }

    public void setLengthMin(String lengthMin) {
        this.lengthMin = lengthMin;
    }

    public String getLengthMax() {
        return lengthMax;
    }

    public void setLengthMax(String lengthMax) {
        this.lengthMax = lengthMax;
    }

    public AccessLevel getAccessLevelRead() {
        return accessLevelRead;
    }

    public void setAccessLevelRead(AccessLevel accessLevelRead) {
        this.accessLevelRead = accessLevelRead;
    }

    public AccessLevel getAccessLevelWrite() {
        return accessLevelWrite;
    }

    public void setAccessLevelWrite(AccessLevel accessLevelWrite) {
        this.accessLevelWrite = accessLevelWrite;
    }

    public Boolean getDisplayReport() {
        return displayReport;
    }

    public void setDisplayReport(Boolean displayReport) {
        this.displayReport = displayReport;
    }

    public Boolean getDisplayUpdate() {
        return displayUpdate;
    }

    public void setDisplayUpdate(Boolean displayUpdate) {
        this.displayUpdate = displayUpdate;
    }

    public Boolean getDisplayResolved() {
        return displayResolved;
    }

    public void setDisplayResolved(Boolean displayResolved) {
        this.displayResolved = displayResolved;
    }

    public Boolean getDisplayClosed() {
        return displayClosed;
    }

    public void setDisplayClosed(Boolean displayClosed) {
        this.displayClosed = displayClosed;
    }

    public Boolean getRequireReport() {
        return requireReport;
    }

    public void setRequireReport(Boolean requireReport) {
        this.requireReport = requireReport;
    }

    public Boolean getRequireUpdate() {
        return requireUpdate;
    }

    public void setRequireUpdate(Boolean requireUpdate) {
        this.requireUpdate = requireUpdate;
    }

    public Boolean getRequireResolved() {
        return requireResolved;
    }

    public void setRequireResolved(Boolean requireResolved) {
        this.requireResolved = requireResolved;
    }

    public Boolean getRequireClosed() {
        return requireClosed;
    }

    public void setRequireClosed(Boolean requireClosed) {
        this.requireClosed = requireClosed;
    }

}
