/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.project.customfields.converter.factory;

import org.squashtest.tm.bugtracker.advanceddomain.InputType;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.project.customfields.CustomFieldsType;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.project.customfields.converter.BasicFieldConverter;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.project.customfields.converter.CollectionFieldConverter;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.project.customfields.converter.CustomFieldConverterType;

import java.util.Map;

import static java.util.Map.entry;

import java.util.Optional;

public class CustomFieldConverterFactory {

    private final Map<String, CustomFieldConverterType> converters;

    public CustomFieldConverterFactory() {
        this.converters = Map.ofEntries(
                entry(CustomFieldsType.TEXT_FIELD,
                      new CustomFieldConverterType(new BasicFieldConverter(), InputType.TypeName.TEXT_FIELD.value)),
                entry(CustomFieldsType.NUMERIC_FIELD,
                      new CustomFieldConverterType(new BasicFieldConverter(), InputType.TypeName.INTEGER.value)),
                entry(CustomFieldsType.FLOAT_FIELD,
                      new CustomFieldConverterType(new BasicFieldConverter(), InputType.TypeName.DECIMAL.value)),
                entry(CustomFieldsType.ENUM_FIELD,
                      new CustomFieldConverterType(new CollectionFieldConverter(),
                                                   InputType.TypeName.DROPDOWN_LIST.value)),
                entry(CustomFieldsType.EMAIL_FIELD,
                      new CustomFieldConverterType(new BasicFieldConverter(), InputType.TypeName.TEXT_FIELD.value)),
                entry(CustomFieldsType.CHECKBOX_FIELD,
                      new CustomFieldConverterType(new CollectionFieldConverter(),
                                                   InputType.TypeName.CHECKBOX_LIST.value)),
                entry(CustomFieldsType.LIST_FIELD,
                      new CustomFieldConverterType(new CollectionFieldConverter(),
                                                   InputType.TypeName.DROPDOWN_LIST.value)),
                entry(CustomFieldsType.MULTI_SELECTION_FIELD,
                      new CustomFieldConverterType(new CollectionFieldConverter(),
                                                   InputType.TypeName.MULTI_SELECT.value)),
                entry(CustomFieldsType.DATE_FIELD,
                      new CustomFieldConverterType(new BasicFieldConverter(), InputType.TypeName.DATE_PICKER.value)),
                entry(CustomFieldsType.RADIO_FIELD,
                      new CustomFieldConverterType(new CollectionFieldConverter(),
                                                   InputType.TypeName.RADIO_BUTTON.value)),
                entry(CustomFieldsType.TEXTAREA_FIELD,
                      new CustomFieldConverterType(new BasicFieldConverter(), InputType.TypeName.TEXT_AREA.value))
                                       );
    }

    public CustomFieldConverterType get(String type) {
        return Optional.ofNullable(converters.get(type)).orElseThrow(
                () -> new IllegalArgumentException(type + " does not exist as a custom field type!"));
    }

}
