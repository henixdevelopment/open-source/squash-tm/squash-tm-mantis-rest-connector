/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis;

import org.springframework.stereotype.Service;
import org.squashtest.csp.core.bugtracker.spi.BugTrackerProviderDescriptor;
import org.squashtest.tm.core.foundation.exception.NullArgumentException;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.service.spi.AdvancedBugTrackerConnector;
import org.squashtest.tm.service.spi.AdvancedBugTrackerConnectorProvider;

import javax.inject.Provider;

@Service("squashtest.core.bugtracker.MantisConnectorProvider")
public class MantisBugtrackerConnectorProvider implements AdvancedBugTrackerConnectorProvider {

    private static final String KIND = "mantis";

    private static final String LABEL = "Mantis Bugtracker REST connector";

    private final Provider<MantisBugtrackerConnector> connectorProvider;

    public MantisBugtrackerConnectorProvider(Provider<MantisBugtrackerConnector> connectorProvider) {
        this.connectorProvider = connectorProvider;
    }

    @Override
    public String getBugTrackerKind() {
        return KIND;
    }

    @Override
    public String getLabel() {
        return LABEL;
    }

    @Override
    public AdvancedBugTrackerConnector createConnector(BugTracker bugTracker) {
        if (bugTracker == null) {
            throw new NullArgumentException("bugTracker");
        }

        MantisBugtrackerConnector connector = connectorProvider.get();
        connector.setBugtracker(bugTracker);
        return connector;
    }

    @Override
    public BugTrackerProviderDescriptor getDescriptor() {
        return new BugTrackerProviderDescriptor() {
            @Override
            public boolean usePathToProjects() {
                return false;
            }
        };
    }

}
