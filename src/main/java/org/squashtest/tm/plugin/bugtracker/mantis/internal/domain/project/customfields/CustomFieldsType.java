/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.project.customfields;

public final class CustomFieldsType {
    private CustomFieldsType() {
    }

    public static final String TEXT_FIELD = "string";
    public static final String NUMERIC_FIELD = "numeric";
    public static final String FLOAT_FIELD = "float";
    public static final String ENUM_FIELD = "enum";
    public static final String EMAIL_FIELD = "email";
    public static final String CHECKBOX_FIELD = "checkbox";
    public static final String LIST_FIELD = "list";
    public static final String MULTI_SELECTION_FIELD = "multilist";
    public static final String DATE_FIELD = "date";
    public static final String RADIO_FIELD = "9"; // 9 is type that mantis rest returns for radio button.
    public static final String TEXTAREA_FIELD = "textarea";
}
