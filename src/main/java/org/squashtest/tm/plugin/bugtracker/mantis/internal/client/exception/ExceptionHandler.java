/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.client.exception;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.squashtest.csp.core.bugtracker.core.BugTrackerNoCredentialsDetailedException;
import org.squashtest.csp.core.bugtracker.core.BugTrackerRemoteException;

import javax.inject.Named;
import java.io.IOException;

import static org.springframework.context.i18n.LocaleContextHolder.getLocale;

@Component("mantisExceptionHandler")
public class ExceptionHandler extends Exception {

    private final transient MessageSource messageSource;

    public ExceptionHandler(@Named("mantisConnectorMessageSource") MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public BugTrackerRemoteException mantisHttpError(HttpClientErrorException ex) {
        final String jsonBody = ex.getResponseBodyAsString();

        try {
            MantisError error =
                    new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                            .readValue(jsonBody, MantisError.class);

            return new MantisException(error);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return genericError(ex);
    }

    public BugTrackerRemoteException genericError(Exception ex) {
        String translation = messageSource.getMessage(ExceptionMessageKeys.UNKNOWN_EXCEPTION, null, getLocale());
        return new BugTrackerRemoteException(String.format("%s %s", translation, ex.getMessage()), ex.getCause());
    }

    public BugTrackerNoCredentialsDetailedException connexionDenied() {
        String translation = messageSource.getMessage(ExceptionMessageKeys.CONNEXION_DENIED, null, getLocale());
        return new BugTrackerNoCredentialsDetailedException(translation, null);
    }

    public BugTrackerRemoteException projectNotFound(String projectName) {
        String translation =
                messageSource.getMessage(ExceptionMessageKeys.PROJECT_NOT_FOUND, new Object[] {projectName},
                        getLocale());
        return new BugTrackerRemoteException(translation, null);
    }

    public BugTrackerRemoteException issueNotFound(String workItemId) {
        String translation =
                messageSource.getMessage(ExceptionMessageKeys.ISSUE_NOT_FOUND, new Object[] {workItemId},
                        getLocale());
        return new BugTrackerRemoteException(translation, null);
    }

    public BugTrackerRemoteException issueNotFound() {
        String translation =
                messageSource.getMessage(ExceptionMessageKeys.ISSUE_NOT_FOUND, null, getLocale());
        return new BugTrackerRemoteException(translation, null);
    }

    public BugTrackerRemoteException cantUploadFiles(){
        String translation = messageSource.getMessage(ExceptionMessageKeys.CANT_UPLOAD_FILE, null, getLocale());
        return new BugTrackerRemoteException (translation,null);
    }

}
