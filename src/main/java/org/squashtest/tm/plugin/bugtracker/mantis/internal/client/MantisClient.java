/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.client;

import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.*;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.response.MantisConfigsResponse;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.response.MantisIssueResponse;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.response.MantisIssuesResponse;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.response.MantisProjectsResponse;

public interface MantisClient {

    void init(BugTracker bugtracker,
              CredentialHolder credentialHolder);

    MantisIssuesResponse findIssues(Long issueId, CredentialHolder credentialHolder);

    MantisProjectsResponse findProjectById(String id, CredentialHolder credentialHolder);

    MantisProjectsResponse findAllProjects(CredentialHolder credentialHolder);

    MantisConfigsResponse getSeverities(CredentialHolder credentialHolder);

    MantisConfigsResponse getPriorities(CredentialHolder credentialHolder);

    MantisConfigsResponse getReproducibilities(CredentialHolder credentialHolder);

    MantisIssueResponse createIssue(Issue issue, CredentialHolder credentialHolder);

    MantisIssuesResponse uploadFiles(String id, MantisFiles files, CredentialHolder credentialHolder);
}
