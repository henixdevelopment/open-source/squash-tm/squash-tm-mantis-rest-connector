/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.advancedissue.customfields.defaultvalue.setter;

import org.squashtest.tm.bugtracker.advanceddomain.AdvancedIssue;
import org.squashtest.tm.bugtracker.advanceddomain.FieldValue;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.ProjectCustomField;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CollectionDefaultValueSetter implements DefaultValueSetter {
    @Override
    public void setDefaultValue(ProjectCustomField field, AdvancedIssue issue) {
        final String defaultValue = field.getDefaultValue();
        if (!defaultValue.isEmpty()) {
            FieldValue fieldValue = new FieldValue(field.getName(), null);
            List<String> components = Arrays.asList(defaultValue.split("\\|"));
            List<FieldValue> fieldValues = new ArrayList<>();
            components.forEach(component ->
                fieldValues.add(new FieldValue(component, component))
            );
            fieldValue.setComposite(fieldValues.toArray(FieldValue[]::new));
            issue.setFieldValue(field.getName(), fieldValue);
        }
    }
}
