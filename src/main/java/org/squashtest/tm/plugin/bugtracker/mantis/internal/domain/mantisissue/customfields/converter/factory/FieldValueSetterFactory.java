/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.mantisissue.customfields.converter.factory;

import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.mantisissue.customfields.converter.*;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.project.customfields.CustomFieldsType;

import java.util.Map;
import java.util.Optional;

public class FieldValueSetterFactory {

    private final Map<String, FieldValueSetter> setters;

    public FieldValueSetterFactory() {
        this.setters = Map.of(
                CustomFieldsType.DATE_FIELD, new DateFieldValueSetter(),
                CustomFieldsType.CHECKBOX_FIELD, new CheckboxFieldValueSetter(),
                CustomFieldsType.MULTI_SELECTION_FIELD, new MultiFieldValueSetter()
        );
    }

    public FieldValueSetter get(String type) {
        return Optional.ofNullable(setters.get(type)).orElse(new DefaultFieldValueSetter());
    }
}
