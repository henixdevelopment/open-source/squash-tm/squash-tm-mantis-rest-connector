/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.utils;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;

@Component("org.squashtest.tm.plugin.mantis.RestTemplateDefaultFactory")
@ConditionalOnProperty(prefix = "plugin.bugtracker.mantis", name = "deactivate-ssl-domain-name-check", havingValue = "false", matchIfMissing = true)
public class RestTemplateDefaultFactory extends RestTemplateAbstractFactory {

    @Override
    protected void initSpringRequestFactory() {
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create().useSystemProperties().disableCookieManagement();

        CloseableHttpClient httpClient = httpClientBuilder.build();

        springRequestFactory = () -> new HttpComponentsClientHttpRequestFactory(httpClient);
    }

}
