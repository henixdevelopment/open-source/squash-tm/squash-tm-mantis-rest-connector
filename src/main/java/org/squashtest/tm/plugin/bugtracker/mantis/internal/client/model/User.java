/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.template.NameTemplate;

@JsonIgnoreProperties(ignoreUnknown = true)
public class User extends NameTemplate {

    @JsonProperty("real_name")
    private String realName;

    @JsonProperty
    private String email;

    @JsonProperty("access_level")
    private UserAccessLevel accessLevel;

    public User() {
    }

    public User(String name) {
        super.name = name;
    }

    public String getEmail() {
        return email;
    }

    public UserAccessLevel getAccessLevel() {
        return accessLevel;
    }

}
