/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.service;

import org.squashtest.tm.bugtracker.advanceddomain.AdvancedIssue;
import org.squashtest.tm.bugtracker.advanceddomain.RemoteIssueSearchRequest;
import org.squashtest.tm.bugtracker.definition.Attachment;
import org.squashtest.tm.bugtracker.definition.RemoteIssue;
import org.squashtest.tm.bugtracker.definition.context.RemoteIssueContext;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.CredentialHolder;

import java.util.List;
import java.util.Optional;

public interface MantisService {

    void init(BugTracker bugtracker, CredentialHolder credentialsHolder);

    List<AdvancedIssue> findKnownIssues(List<String> list, CredentialHolder credentialHolder);

    Optional<AdvancedIssue> searchIssue(RemoteIssueSearchRequest searchRequest, CredentialHolder credentialHolder);

    RemoteIssue createReportIssueTemplate(String projectName, RemoteIssueContext context, CredentialHolder credentialHolder, BugTracker bugTracker);

    AdvancedIssue createIssue(RemoteIssue remoteIssue, CredentialHolder credentialHolder);

    void forwardAttachments(String issueKey, List<Attachment> attachments, CredentialHolder credentialHolder);
}
