/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.client;

import java.util.Arrays;

public class PathBuilder {

    private final String uri;

    public static final String ROOT_API = "/api/rest";

    public static final String DELIMITER ="/";

    public static final String ISSUES = "issues";

    public static final String CONFIG_OPTION = "config?option=";

    private PathBuilder(String uri) {
        this.uri = uri;
    }

    public static PathBuilder buildCurrentUserPath() {
        return new PathBuilder(String.join(DELIMITER, Arrays.asList(ROOT_API, "users/me")));
    }

    public static PathBuilder buildIssuePath(String issueId) {
        return new PathBuilder(String.join(DELIMITER, Arrays.asList(ROOT_API, ISSUES, issueId)));
    }

    public static PathBuilder buildAllProjectsPath() {
        return new PathBuilder(String.join(DELIMITER, Arrays.asList(ROOT_API, "projects")));
    }

    public static PathBuilder buildProjectPath(String projectId) {
        return new PathBuilder(String.join(DELIMITER, Arrays.asList(ROOT_API, "projects", projectId)));
    }

    public static PathBuilder buildSeveritiesPath() {
        return new PathBuilder(String.join(DELIMITER, Arrays.asList(ROOT_API, CONFIG_OPTION + "severity_enum_string")));
    }

    public static PathBuilder buildPrioritiesPath() {
        return new PathBuilder(String.join(DELIMITER, Arrays.asList(ROOT_API, CONFIG_OPTION + "priority_enum_string")));
    }

    public static PathBuilder buildReproducibilityPath() {
        return new PathBuilder(String.join(DELIMITER, Arrays.asList(ROOT_API, CONFIG_OPTION + "reproducibility_enum_string")));
    }

    public static PathBuilder buildAddIssuePath() {
        return new PathBuilder(String.join(DELIMITER, Arrays.asList(ROOT_API, ISSUES)));
    }

    public static PathBuilder buildAttachmentsPath(String issueId) {
        return new PathBuilder(String.join(DELIMITER, Arrays.asList(ROOT_API, ISSUES, issueId, "files")));
    }

    public String build() {
        return this.uri;
    }

}
