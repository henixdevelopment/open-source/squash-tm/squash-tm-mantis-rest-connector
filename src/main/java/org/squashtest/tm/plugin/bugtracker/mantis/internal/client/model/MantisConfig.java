/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.template.LabelTemplate;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MantisConfig {

    @JsonProperty
    private String option;

    @JsonProperty("value")
    private List<LabelTemplate> values;

    public MantisConfig() {
    }

    public MantisConfig(List<LabelTemplate> values) {
        this.values = values;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public List<LabelTemplate> getValues() {
        return values;
    }

    public void setValues(List<LabelTemplate> values) {
        this.values = values;
    }

}
