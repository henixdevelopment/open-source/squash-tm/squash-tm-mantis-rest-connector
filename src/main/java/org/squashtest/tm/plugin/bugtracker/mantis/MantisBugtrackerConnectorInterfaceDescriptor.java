/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.squashtest.csp.core.bugtracker.spi.BugTrackerInterfaceDescriptor;

import javax.inject.Named;
import java.util.Locale;

@Component
public class MantisBugtrackerConnectorInterfaceDescriptor implements BugTrackerInterfaceDescriptor {

    private static final String REPORT_PRIORITY_LABEL  	 = "interface.report.priority.label";
    private static final String REPORT_SEVERITY_LABEL  	 = "interface.report.severity.label";
    private static final String REPORT_VERSION_LABEL  	 = "interface.report.version.label";
    private static final String REPORT_ASSIGNEE_LABEL 	 = "interface.report.assignee.label";
    private static final String REPORT_CATEGORY_LABEL 	 = "interface.report.category.label";
    private static final String REPORT_SUMMARY_LABEL 	 = "interface.report.summary.label";
    private static final String REPORT_DESCRIPTION_LABEL = "interface.report.description.label";
    private static final String REPORT_COMMENT_LABEL 	 = "interface.report.comment.label";

    private static final String REPORT_EMPTY_VERSION	 = "interface.report.lists.emptyversion.label";
    private static final String REPORT_EMPTY_CATEGORY	 = "interface.report.lists.emptycategory.label";
    private static final String REPORT_EMPTY_ASSIGNEE	 = "interface.report.lists.emptyassignee.label";

    private static final String TABLE_ISSUE_ID_HEADER = "interface.table.issueid.header";
    private static final String TABLE_SUMMARY_HEADER	 = "interface.table.summary.header";
    private static final String TABLE_PRIORITY_HEADER	 = "interface.table.priority.header";
    private static final String TABLE_STATUS_HEADER		 = "interface.table.status.header";
    private static final String TABLE_ASSIGNEE_HEADER	 = "interface.table.assignee.header";
    private static final String TABLE_REPORTED_IN_HEADER = "interface.table.reportedin.header";
    private static final String TABLE_DESCRIPTION_HEADER = "interface.table.description.header";

    private static final String TABLE_EMPTY_ASSIGNEE	 = "interface.table.null.assignee.label";

    private Locale locale;

    private final MessageSource messageSource;

    public MantisBugtrackerConnectorInterfaceDescriptor(
            @Named("mantisConnectorMessageSource") MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public Locale getLocale() {
        return locale;
    }

    @Override
    public boolean getSupportsRichDescription() {
        return false;
    }

    @Override
    public boolean getSupportsRichComment() {
        return false;
    }

    @Override
    public String getReportPriorityLabel() {
        return messageSource.getMessage(REPORT_PRIORITY_LABEL, null, getLocale());
    }

    public String getReportSeverityLabel() {
        return messageSource.getMessage(REPORT_SEVERITY_LABEL, null, getLocale());
    }

    @Override
    public String getReportVersionLabel() {
        return messageSource.getMessage(REPORT_VERSION_LABEL, null, getLocale());
    }

    @Override
    public String getReportAssigneeLabel() {
        return messageSource.getMessage(REPORT_ASSIGNEE_LABEL, null, getLocale());
    }

    @Override
    public String getReportCategoryLabel() {
        return messageSource.getMessage(REPORT_CATEGORY_LABEL, null, getLocale());
    }

    @Override
    public String getReportSummaryLabel() {
        return messageSource.getMessage(REPORT_SUMMARY_LABEL, null, getLocale());
    }

    @Override
    public String getReportDescriptionLabel() {
        return messageSource.getMessage(REPORT_DESCRIPTION_LABEL, null, getLocale());
    }

    @Override
    public String getReportCommentLabel() {
        return messageSource.getMessage(REPORT_COMMENT_LABEL, null, getLocale());
    }

    @Override
    public String getEmptyVersionListLabel() {
        return messageSource.getMessage(REPORT_EMPTY_VERSION, null, getLocale());
    }

    @Override
    public String getEmptyCategoryListLabel() {
        return messageSource.getMessage(REPORT_EMPTY_CATEGORY, null, getLocale());
    }

    @Override
    public String getEmptyAssigneeListLabel() {
        return messageSource.getMessage(REPORT_EMPTY_ASSIGNEE, null, getLocale());
    }

    @Override
    public String getTableIssueIDHeader() {
        return messageSource.getMessage(TABLE_ISSUE_ID_HEADER, null, getLocale());
    }

    @Override
    public String getTableSummaryHeader() {
        return messageSource.getMessage(TABLE_SUMMARY_HEADER, null, getLocale());
    }

    @Override
    public String getTablePriorityHeader() {
        return messageSource.getMessage(TABLE_PRIORITY_HEADER, null, getLocale());
    }

    @Override
    public String getTableStatusHeader() {
        return messageSource.getMessage(TABLE_STATUS_HEADER, null, getLocale());
    }

    @Override
    public String getTableDescriptionHeader() {
        return messageSource.getMessage(TABLE_DESCRIPTION_HEADER, null, getLocale());
    }

    @Override
    public String getTableAssigneeHeader() {
        return messageSource.getMessage(TABLE_ASSIGNEE_HEADER, null, getLocale());
    }

    @Override
    public String getTableReportedInHeader() {
        return messageSource.getMessage(TABLE_REPORTED_IN_HEADER, null, getLocale());
    }

    @Override
    public String getTableNoAssigneeLabel() {
        return messageSource.getMessage(TABLE_EMPTY_ASSIGNEE, null, getLocale());
    }

}
