/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.mantisissue;

import org.squashtest.tm.bugtracker.advanceddomain.AdvancedIssue;
import org.squashtest.tm.bugtracker.advanceddomain.FieldValue;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Issue;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Project;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Category;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Severity;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.User;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Priority;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Reproducibility;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.ViewState;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Version;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Tag;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.Field;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.IssueCustomField;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.ProjectCustomField;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.mantisissue.customfields.converter.FieldValueSetter;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.mantisissue.customfields.converter.factory.FieldValueSetterFactory;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.FieldIds;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.project.customfields.SchemeBuilder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SquashToMantisIssueConverter {

    private static final FieldValueSetterFactory factory = new FieldValueSetterFactory();

    public static Issue convert(AdvancedIssue advancedIssue, Project project) {
        Issue issue = new Issue();

        issue.setProject(project);
        issue.setCategory(new Category(extractCategory(advancedIssue)));
        issue.setSeverity(new Severity(extractSeverity(advancedIssue)));
        issue.setHandler(new User(extractAssignee(advancedIssue)));
        issue.setSummary(extractSummary(advancedIssue));
        issue.setDescription(extractDescription(advancedIssue));
        issue.setAdditionalInformation(extractAdditionalInformation(advancedIssue));
        issue.setPriority(new Priority(extractPriority(advancedIssue)));
        issue.setReproducibility(new Reproducibility(extractReproducibility(advancedIssue)));
        issue.setStepsToReproduce(extractStepsToReproduce(advancedIssue));
        issue.setVisibility(new ViewState(extractVisibility(advancedIssue)));
        issue.setTags(extractTags(advancedIssue));
        issue.setCustomFields(extractCUF(advancedIssue, project.getCustomFields()));

        FieldValue versionFieldValue = extractVersionFieldValue(advancedIssue);
        if (!versionFieldValue.getId().equals(SchemeBuilder.NONE_OPTION)) {
            issue.setVersion(new Version(versionFieldValue.getScalar()));
        }

        return issue;

    }


    private SquashToMantisIssueConverter() {
    }

    private static String extractCategory(AdvancedIssue advancedIssue) {
        return advancedIssue.getFieldValue(FieldIds.CATEGORY_FIELD_ID).getScalar();
    }

    private static String extractSeverity(AdvancedIssue advancedIssue) {
        return advancedIssue.getFieldValue(FieldIds.SEVERITY_FIELD_ID).getId();
    }

    private static String extractAssignee(AdvancedIssue advancedIssue) {
        return advancedIssue.getFieldValue(FieldIds.ASSIGNEE_FIELD_ID).getScalar();
    }

    private static String extractSummary(AdvancedIssue advancedIssue) {
        return advancedIssue.getFieldValue(FieldIds.SUMMARY_FIELD_ID).getScalar();
    }

    private static String extractDescription(AdvancedIssue advancedIssue) {
        return advancedIssue.getFieldValue(FieldIds.DESCRIPTION_FIELD_ID).getScalar();
    }

    private static String extractAdditionalInformation(AdvancedIssue advancedIssue) {
        return advancedIssue.getFieldValue(FieldIds.ADDITIONAL_INFORMATION_FIELD_ID).getScalar();
    }

    private static FieldValue extractVersionFieldValue(AdvancedIssue advancedIssue) {
        return advancedIssue.getFieldValue(FieldIds.VERSION_FIELD_ID);
    }


    private static String extractPriority(AdvancedIssue advancedIssue) {
        return advancedIssue.getFieldValue(FieldIds.PRIORITY_FIELD_ID).getId();
    }

    private static String extractReproducibility(AdvancedIssue advancedIssue) {
        return advancedIssue.getFieldValue(FieldIds.REPRODUCIBILITY_FIELD_ID).getId();
    }

    private static String extractStepsToReproduce(AdvancedIssue advancedIssue) {
        return advancedIssue.getFieldValue(FieldIds.STEPS_TO_REPRODUCE_FIELD_ID).getScalar();
    }

    private static String extractVisibility(AdvancedIssue advancedIssue) {
        return advancedIssue.getFieldValue(FieldIds.VIEW_STATE_FIELD_ID).getScalar().equals("true") ? "public" : "private";
    }

    private static List<Tag> extractTags(AdvancedIssue advancedIssue) {
        FieldValue[] response = advancedIssue.getFieldValue(FieldIds.TAGS_FIELD_ID).getComposite();
        return Arrays.stream(response).map(fieldValue -> new Tag(fieldValue.getScalar())).collect(Collectors.toList());
    }

    private static List<IssueCustomField> extractCUF(AdvancedIssue advancedIssue, List<ProjectCustomField> customFields) {
        List<IssueCustomField> result = new ArrayList<>();

        customFields.forEach(field -> {
            FieldValue fieldValue = advancedIssue.getFieldValue(field.getName());
            FieldValueSetter setter = factory.get(field.getType());
            setter.set(fieldValue);
            result.add(new IssueCustomField(new Field(field.getName()), fieldValue.getScalar()));
        });
        return result;
    }

}
