/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.advancedissue;

import org.squashtest.tm.bugtracker.advanceddomain.AdvancedIssueReportForm;
import org.squashtest.tm.bugtracker.advanceddomain.FieldValue;

import java.util.HashMap;
import java.util.Map;

public class MantisAdvancedIssue extends AdvancedIssueReportForm {

    public static MantisAdvancedIssue from(AdvancedIssueReportForm advancedIssue) {
        MantisAdvancedIssue mantisAdvancedIssue = new MantisAdvancedIssue();
        mantisAdvancedIssue.setAdditionalData(advancedIssue.getAdditionalData());
        mantisAdvancedIssue.setBugtracker(advancedIssue.getBugtracker());
        mantisAdvancedIssue.setComment(advancedIssue.getComment());
        mantisAdvancedIssue.setCurrentScheme(advancedIssue.getCurrentScheme());
        mantisAdvancedIssue.setDescription(advancedIssue.getDescription());
        mantisAdvancedIssue.setId(advancedIssue.getId());
        mantisAdvancedIssue.setProject(advancedIssue.getProject());
        mantisAdvancedIssue.setSummary(advancedIssue.getSummary());

        Map<String, FieldValue> fieldValuesCopy = new HashMap<>(advancedIssue.getFieldValues());
        mantisAdvancedIssue.setFieldValues(fieldValuesCopy);

        return mantisAdvancedIssue;
    }

    @Override
    public String getRemoteKey() {
        return this.getId();
    }
}
