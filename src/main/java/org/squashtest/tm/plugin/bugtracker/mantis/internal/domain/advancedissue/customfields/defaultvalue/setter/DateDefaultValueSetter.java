/*
 * This file is part of the Squashtest platform.
 * Copyright (C) 2022 Henix, henix.fr
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * this software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.advancedissue.customfields.defaultvalue.setter;

import org.apache.commons.validator.GenericValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.tm.bugtracker.advanceddomain.AdvancedIssue;
import org.squashtest.tm.bugtracker.advanceddomain.FieldValue;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.client.model.ProjectCustomField;
import org.squashtest.tm.plugin.bugtracker.mantis.internal.domain.mantisissue.customfields.converter.DateFieldValueSetter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class DateDefaultValueSetter implements DefaultValueSetter {

    private static final Logger LOGGER = LoggerFactory.getLogger(DateFieldValueSetter.class);
    private static final List<String> dateFormats = Arrays.asList("yyyy-MM-dd", "yyyy/MM/dd", "yyyyMMdd", "dd-MM-yyyy", "dd/MM/yyyy");

    @Override
    public void setDefaultValue(ProjectCustomField field, AdvancedIssue issue) {
        final String defaultValue = field.getDefaultValue();
        if(!defaultValue.isEmpty()) {
            try {
                String formattedDate = new SimpleDateFormat("yyyy-MM-dd").format(getCorrectDateFormat(defaultValue));
                issue.setFieldValue(field.getName(), new FieldValue(formattedDate, formattedDate));
            } catch(ParseException e) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Invalid date format.", e);
                }
            }
        }
    }

    private Date getCorrectDateFormat(String date) throws ParseException {
        String defaultDateFormat = "";
        for (String format : dateFormats) {
            if (GenericValidator.isDate(date, format, true)) {
                defaultDateFormat = format;
            }
        }
        return new SimpleDateFormat(defaultDateFormat).parse(date);
    }
}
